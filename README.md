<h1 align="center">
    &#127760; Virtual-Tour-Builder
</h1>
<p align="center">
    <img src="assets/banner.png" />
    <br>
    (Panorama: Parc national du Mercantour © Damien Sorel)
</p>

<h4 align="center"> Create and host your own 360° virtual tour via a very simple web interface supporting multiple different tours. </h4>

<p align="center">
  <a href="https://badgen.net/">
    <img src="https://flat.badgen.net/badge/-/TypeScript?icon=typescript&label&labelColor=blue&color=555555" alt="Typescript">
  </a>
</p>

***

## Features
- Support photo & video 360° tours
- Simple admin interface
- Self hosted
- Automatically splits your panorama into tiles
- Add videos with chroma keying, links, infos and polygon markers to your tour
- Manage multiple, possibly interconnected tours
- ...

***
The following sections need a rework as a lot of things recently changed.

## Installation
- Install [Node.js](https://nodejs.org/) (including npm), [FFmpeg](https://ffmpeg.org/) and [GraphicsMagick](http://www.graphicsmagick.org/) by following a guide for your OS.
- Download the latest release from [here](https://codeberg.org/Mr-Kanister/Virtual-Tour-Builder/releases) and extract it to an appropriate directory not exposed to the internet (for example `/var/www/`).
- Follow the next section for opening it up to the internet.
- Start the server on a desired port by `APP_PORT=23200 node dist/server/index.js`

### Exposing to the internet
Even though the project is using [Express](https://expressjs.com/), you need an additional webserver like [Apache](https://apache.org/) or [nginx](https://nginx.org/) installed. 

- Configure a reverse proxy to proxy `/` to the express server (`localhost:23200`).
- Optional: Configure your webserver to set the CSP headers to prevent unsafe-eval and unsafe-inline script sources!

### Embedding into a website
To display your virtual tour, simply add a `<div id="viewer"></div>` with some width and height styling and a `<script type="module" src="/resources/index.js"></script>` to your website ([example](https://codeberg.org/Mr-Kanister/Virtual-Tour-Builder/src/branch/main/dist/frontend/index.html)).

### Creating a systemd service
Normally the server runs in the foreground, which is only appropriate for development purposes. To add a systemd service, there is an [example file](https://codeberg.org/Mr-Kanister/Virtual-Tour-Builder/src/branch/main/virtual-tour-builder.service) which you need to edit in order to fit your environment. You need to change the `USERNAME`'s and `/your/path/to`'s as well as the `ExecStart` in general if you aren't using [nvm](https://github.com/nvm-sh/nvm).

Then copy and run the service:
``` bash
cp virtual-tour-builder.service /etc/systemd/system/
systemctl daemon-reload
systemctl start virtual-tour-builder
systemctl enable virtual-tour-builder
```

***

## Usage
After running the server, you can fire up your browser and point it to your hostname followed by `/admin`. After typing in the default credentials (admin:admin), the Editor is shown.

Switch to the Users tab and add a user with a proper password. Then you can delete the admin:admin user and switch back to the Editor. 

Here you can add, edit and remove nodes to and from your virtual tour.

### 

### ➕ Adding a node
- After clicking on `Add node`, you can choose an ID for this node. The ID is unique and not changeable later on. It is advised to think about a naming scheme as the nodes will be sorted by the ID.
- Next you choose a default display name, and decide if this node should be the start of your tour.
- If you configured a map, you can now choose a location for the node by clicking on the desired spot on the map. If you select the node as a hotspot, a white dot will be displayed on the map and the user will be able to instantly jump to the node by clicking the hotspot.
- Next select a 360° equirectangular image and save the node.

Congratulations, you've just added your first node to the virtual tour! You can view it by visiting just your hostname without any suffix. Now add a second one and learn about links between nodes:

### 🔗 Linking
After you have at least two nodes, you can add a link to a node in the node editor. You can choose the target node id (to which the user jumps, if he/she clicks the link), a display name (if empty, the default one of the node is chosen instead).

### 📍 Add Markers
Clicking `Add Marker` inside of the viewer, you can choose between several marker types:
- An info marker is just an info icon, which displays text (`content`) when clicked on.
- A link is a link.
- When you choose a video, the video will be re-encoded in the background, so the quality of all videos is approximately the same. You can choose to do chroma keying. This is done on the client side.
- After choosing polygon, you can start clicking on the 360° image and thus create a polygon. By right clicking, you can undo your last point and by clicking on the line, you complete it. When a polygon gets clicked by a user, a text (`content`) is displayed.

You can always edit a markers properties by clicking it.

### ⛓ Chaining
If, for example, you want to map a long corridor with 360° images and it would be very annoying for the user to click several images further until he/she has reached the end, but at the same time, you want to show the whole corridor and not just one image at the beginning and one at the end, then you can create a node-chain:
On one of the node, which should be "skipped", you create a new chain and select from which node the chain should be triggered and to which node it should then chain to.

### 💬 Change language
Currently available languages: English (en), German (de). Feel free to add other languages! Select your desired language in the `Settings` tab. This will be server wide.

***

## Development
For creating a development environment simply clone the repo, cd into it and install all the packages by `npm install`. Now you can build the source (`npm run build`, `npm run build-frontend`, `npm run build-backend`), watch it (`npm run watch-frontend`, `npm run watch-backend`) and run the server (`npm run server`).

Currently you still need a webserver (Apache/nginx/...), even when developing.

***

## Credits
The whole project relies heavily on the excellent work on [PhotoSphereViewer](https://photo-sphere-viewer.js.org) by [mistic100](https://github.com/mistic100) and all contributers.

Furthermore this project uses the following open source packages:
- [Typescript](https://www.typescriptlang.org/)
- [Node.js](https://nodejs.org/)
- [Webpack](https://webpack.js.org/)
- [Express](https://expressjs.com/)
- [Ajv JSON schema validator](https://ajv.js.org/)
- [FFmpeg](https://ffmpeg.org/), [fluent-ffmpeg](https://github.com/fluent-ffmpeg/node-fluent-ffmpeg)
- [GraphicsMagick](http://www.graphicsmagick.org/), [gm](https://www.npmjs.com/package/gm)
- [lowdb](https://www.npmjs.com/package/lowdb)
- [bcryptjs](https://www.npmjs.com/package/bcryptjs)
- [html-entities](https://www.npmjs.com/package/html-entities)
- [simple-node-logger](https://www.npmjs.com/package/simple-node-logger)
- [sprintf-js](https://www.npmjs.com/package/sprintf-js)

***

## Disclaimer
The login system implemented is a very simple one and I do not know at this stage if it has any security flaws. If there is a risk of losing sensitive information if a third party gains access to the admin page, please consider checking the code sections responsible for the access.against your security standards.

To the best of my current knowledge, the system is secure. Unauthorised access cannot reveal any additional information, only unauthorised deletion of information is possible.

***

## License
GNU GPLv3 