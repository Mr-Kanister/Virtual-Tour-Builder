import MiniCssExtractPlugin from "mini-css-extract-plugin";
import CssMinimizerPlugin from "css-minimizer-webpack-plugin";
import CopyPlugin from "copy-webpack-plugin";
import path, { dirname } from 'node:path';
import { fileURLToPath } from 'node:url';

const __dirname = dirname(fileURLToPath(import.meta.url));

// const glob = require("glob");

// frontend config
export default {
    devtool: "source-map",
    // entry: glob.sync("**.ts", { cwd: "./src/frontend/" }).reduce((obj, cur) => {
    // 	obj["resources/" + cur] = "./src/frontend/" + cur;

    // 	return obj;
    // }, {}),
    entry: {
        "tours/home": "./src/app/tours/home.ts",
        "tours/dashboard": "./src/app/tours/dashboard.ts",
        "tours/settings": "./src/app/tours/settings.ts",
        "tours/settings/start-position": "./src/app/tours/settings/start-position.ts",
        "tours/settings/navbar": "./src/app/tours/settings/navbar.ts",
        "nodes/list": "./src/app/nodes/list.ts",
        "nodes/info": "./src/app/nodes/info.ts",
        "nodes/settings/links": "./src/app/nodes/settings/links.ts",
        "nodes/settings/chains": "./src/app/nodes/settings/chains.ts",
        "nodes/settings/sphere-correction": "./src/app/nodes/settings/sphere-correction.ts",
        "nodes/settings/markers": "./src/app/nodes/settings/markers.ts",
        "nodes/settings/location": "./src/app/nodes/settings/location.ts",
        "admin/users": "./src/app/admin/users.ts",
        "layout": "./src/app/layout.ts",
        "auth/auth": "./src/app/auth/auth.ts",
        "index": "./src/app/index.ts",
        "markers/list": "./src/app/markers/list.ts",
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: [{
                    loader: "ts-loader",
                    options: {
                        configFile: "tsconfig-app.json"
                    }
                }]
            },
            {
                test: /\.scss$/,
                use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
            }
        ]
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "dist", "public", "resources"),
        publicPath: ""
    },
    mode: "production",
    resolve: {
        extensions: [
            ".ts",
            ".js",
            ".css",
            ".scss"
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css",
        }),
        new CopyPlugin({
            patterns: [
                {
                    from: "**/*.css*",
                    context: "node_modules/@photo-sphere-viewer/",
                    to: "node_modules_css/"
                },
                {
                    from: "*.css*",
                    context: "node_modules/leaflet/dist/",
                    to: "node_modules_css",
                  },
            ],
        }),
    ],
    optimization: {
        minimizer: [
            new CssMinimizerPlugin(),
        ],
    },
}