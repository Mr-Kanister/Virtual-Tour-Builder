import express from "express";
import { ensureLoggedIn } from "connect-ensure-login";
import { db } from "../index.js";
import { ViewerConfig } from "@photo-sphere-viewer/core";
import { MarkerConfig } from "@photo-sphere-viewer/markers-plugin";

export const router = express.Router();

const ensureLogIn = ensureLoggedIn("/auth/login");

router.get("/(tours(/home)?)?", ensureLogIn, (req, res) => {
    const tourId = req.session.currentTour;
    res.render("app/tours/home.ejs", {
        location: "tours",
        tours: db.getDbTours(),
        tour: tourId ? db.getDbTour({ tourId }) : undefined,
    });
});

router.get("/tours/:tour(\\d+)(/dashboard)?", ensureLogIn, (req, res) => {
    req.session.currentTour = Number(req.params.tour);

    res.render("app/tours/dashboard.ejs", {
        location: "dashboard", tour: db.getDbTour({ tourId: Number(req.params.tour) })
    });
});

router.get("/tours/:tour(\\d+)/settings/:setting?", ensureLogIn, (req, res) => {
    req.session.currentTour = Number(req.params.tour);
    const tour = db.getDbTour({ tourId: Number(req.params.tour) });
    const parsedTour = {
        ...tour,
        viewerConfig: JSON.parse(tour.viewerConfig) as ViewerConfig,
    }

    if (req.params.setting === "start-position") {
        res.render("app/tours/settings/start-position.ejs", {
            location: "settings", tour: parsedTour,
        });
    } else if (req.params.setting === "navbar") {
        const navbarActive = parsedTour.viewerConfig.navbar as string[];
        const navbarInactive = [
            "zoomOut",
            "zoomRange",
            "zoomIn",
            "moveLeft",
            "moveRight",
            "moveTop",
            "moveDown",
            "download",
            "description",
            "caption",
            "fullscreen",
            "markers",
            "markersList",
            ...tour.type === "equirectangular-video" ? [
                "videoPlay",
                "videoVolume",
                "videoTime",
            ] : [],
        ].filter(x => !navbarActive.includes(x));

        res.render("app/tours/settings/navbar.ejs", {
            location: "settings",
            tour: parsedTour,
            navbarActive,
            navbarInactive,
        });
    } else {
        res.render("app/tours/settings.ejs", {
            location: "settings", tour: parsedTour,
        });
    }
});

router.get("/nodes/:node?/:setting?", ensureLogIn, (req, res) => {
    const nodeId = Number(req.params.node);
    let tourId = req.session.currentTour;
    if (!tourId && !Number.isNaN(nodeId)) {
        res.redirect("/app");
        return;
    }

    if (Number.isNaN(nodeId)) {
        // no node given: list all nodes
        const sortCol = req.query["sort-col"];
        const sortOrder = req.query["sort-order"];

        const nodes = db.getDbNodes(tourId)
            // sort nodes based on query
            .sort((a, b) => {
                let score = 0;
                if (sortCol === "name") {
                    score = (a.name ?? "").localeCompare(b.name ?? "");
                } else if (sortCol === "caption") {
                    score = (a.caption ?? "").localeCompare(b.caption ?? "");
                }

                if (sortOrder === "desc") {
                    score = -score;
                }
                return score;
            });

        const virtualTourConfig = (db.prepare(`SELECT config FROM plugins WHERE tour = ? AND name = ?`)
            .get(tourId, "virtual-tour") as { config: string } | undefined)?.config;

        res.render("app/nodes/list.ejs", {
            location: "nodes",
            tour: tourId ? db.getDbTour({ tourId }) : undefined,
            startNodeId: virtualTourConfig ? JSON.parse(virtualTourConfig).startNodeId : undefined,
            nodes: nodes,
            sortCol,
            sortOrder,
        });
        return;
    }

    let tour;
    if (tourId) {
        tour = db.getDbTour({ tourId });
    } else {
        // set current tour in session if not set
        tour = db.getDbTour({ nodeId });
        req.session.currentTour = tour.id;
    }
    const node = db.getDbNode(nodeId);
    node.data = JSON.parse(node.data ?? "{}");

    if (req.params.setting === "location") {
        res.render("app/nodes/settings/location.ejs", {
            location: "nodes",
            node: node,
            tour,
        });
        return;
    }

    if (req.params.setting === "sphere-correction") {
        res.render("app/nodes/settings/sphere-correction.ejs", {
            location: "nodes",
            tour: tour,
            node: node,
        });
        return;
    }

    if (req.params.setting === "links") {
        const links = db.getDbLinks(nodeId).map(l => {
            l.linkOffset = l.linkOffset ? JSON.parse(l.linkOffset) : undefined
            return l;
        });
        res.render("app/nodes/settings/links.ejs", {
            location: "nodes",
            tour: tour,
            node: node,
            links: links,
            tours: db.getDbTours(),
        });
        return;
    }

    if (req.params.setting === "markers") {
        res.render("app/nodes/settings/markers.ejs", {
            location: "nodes",
            tour: tour,
            node: node,
        });
        return;
    }

    if (req.params.setting === "chains") {
        res.render("app/nodes/settings/chains.ejs", {
            location: "nodes",
            tour: tour,
            node: node,
            chains: db.getChains(nodeId),
        });
        return;
    }
    res.render("app/nodes/info.ejs", {
        location: "nodes",
        tour: tour,
        node: node,
        links: db.getDbLinks(nodeId),
        markers: db.getDbMarkers(nodeId),
        chains: db.getChains(nodeId),
        hotspot: db.getDbHotspot(nodeId),
    });
});

router.get("/admin/users", ensureLogIn, (req, res) => {
    const tourId = req.session.currentTour;
    const users = db.getUsers().map(user => {
        let last_seen = "Never";
        if (user.last_login) {
            const diff = new Date().getTime() - user.last_login;

            if (diff < 60000) {
                // 1 minute
                last_seen = "Now";
            } else if (diff < 3600000) {
                // 60 minutes
                last_seen = `${(diff / (1000 * 60)).toFixed(0)} minutes ago`;
            } else if (diff < 86400000) {
                // 24 hours
                last_seen = `${(diff / (1000 * 60 * 60)).toFixed(0)} hours ago`;
            } else {
                last_seen = `${(diff / (1000 * 60 * 60 * 24)).toFixed(0)} days ago`;
            }
        }
        return {
            ...user,
            last_seen,
        };
    });
    res.render("app/admin/users.ejs", {
        location: "users",
        users: users,
        tour: tourId ? db.getDbTour({ tourId }) : undefined,
    });
});

router.get("/markers", ensureLogIn, (req, res) => {
    const sortCol = req.query["sort-col"];
    const sortOrder = req.query["sort-order"];
    let tourId = req.session.currentTour;
    if (!tourId) {
        res.redirect("/app");
        return;
    }

    const markers = db.prepare(`
        SELECT config, name as node_name, node_id, markers.id
        FROM markers
        LEFT JOIN nodes
        ON markers.node_id = nodes.id
        WHERE tour = ?`).all(tourId) as {
        config: string,
        node_name: string,
        node_id: number,
        id: number,
    }[];

    const parsedMarkers = markers
        // parse marker config
        .map(m => {
            return {
                node_name: m.node_name,
                node_id: m.node_id,
                ...JSON.parse(m.config) as MarkerConfig,
            };
        })
        // add marker type
        .map(m => {
            let type = "";
            if (m.image) {
                type = "Image";
            } else if (m.imageLayer) {
                type = "Embedded image";
            } else if (m.videoLayer) {
                type = "Embedded video";
            } else if (m.data?.iframe) {
                type = "Embedded iFrame";
            } else if (m.html) {
                type = "HTML";
            } else if (m.polygon) {
                type = "Polygon";
            } else if (m.polyline) {
                type = "Polyline";
            }
            return {
                ...m,
                type,
            };
        })
        // sort markers based on query
        .sort((a, b) => {
            let score = 0;
            if (sortCol === "tooltip") {
                score = (typeof a.tooltip === "string" ? a.tooltip : a.tooltip?.content ?? "")
                    .localeCompare((typeof b.tooltip === "string" ? b.tooltip : b.tooltip?.content ?? ""));
            } else if (sortCol === "sidepanel-content") {
                score = (a.content ?? "").localeCompare(b.content ?? "");
            } else if (sortCol === "type") {
                score = a.type.localeCompare(b.type);
            } else if (sortCol === "parent") {
                score = a.node_name.localeCompare(b.node_name);
            }

            if (sortOrder === "desc") {
                score = -score;
            }
            return score;
        });

    res.render("app/markers/list.ejs", {
        location: "markers",
        tour: tourId ? db.getDbTour({ tourId }) : undefined,
        markers: parsedMarkers,
        sortCol,
        sortOrder,
    });
    return;
});