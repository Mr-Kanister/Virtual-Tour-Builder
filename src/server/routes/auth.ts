import passport from "passport"
import { Strategy as LocalStrategy } from "passport-local"
import { Strategy as BearerStrategy } from "passport-http-bearer";
import bcrypt from "bcryptjs";
import express from "express";
import { ensureLoggedInApi } from "../utils/ensureLoggedInApi.js";
import crypto from "node:crypto";
import { ensureLoggedOut } from "connect-ensure-login";
import { db } from "../index.js";
import { DBUser } from "../utils/types.js";

export const router = express.Router();

passport.use(new LocalStrategy(async (username, password, done) => {
  try {
    const user = db.prepare(`SELECT * FROM users WHERE username = ?`).get(username);
    if (!user || typeof user !== "object" ||
      !("username" in user) || !("password" in user) ||
      typeof user.username !== "string") {
      done(null, false, { message: "Incorrect username or password." });
      return;
    }
    if (!(await bcrypt.compare(password, user.password as string))) {
      done(null, false, { message: "Incorrect username or password." });
      return;
    }
    done(null, { username: user.username });
  } catch (error) {
    done(error);
  }
}));

passport.use(new BearerStrategy(async (token, done) => {
  try {
    const tokenParts = token.split("-", 1);
    const keys = db.prepare(`SELECT key FROM api_keys WHERE user = ?`).all(tokenParts[0]) as { key: string }[];
    for (const key of keys) {
      if (await bcrypt.compare(token, key.key)) {
        done(null, { username: tokenParts[0] });
        return;
      }
    }
    done(null, false);
  } catch (error) {
    done(error);
  }
}));

passport.serializeUser((user, done) => {
  process.nextTick(() => {
    done(null, user.username); // TODO
  });
});

passport.deserializeUser((user: string, done) => {
  process.nextTick(() => {
    return done(null, db.getDB().prepare(`SELECT * FROM users WHERE username = ?`).get(user) as DBUser); // TODO
  });
});

router.get("/login", ensureLoggedOut("/app/"), (req, res, next) => {
  res.render("auth/login", { layout: "./components/auth.ejs" });
});

router.post("/login", ensureLoggedOut("/app/"), passport.authenticate("local", {
  successReturnToOrRedirect: "/app/",
  failureRedirect: "/auth/login",
  keepSessionInfo: true
}));

router.get("/logout", (req, res, next) => {
  req.logout(err => {
    if (err) return next(err);
    res.redirect("/auth/login");
  });
});

router.get("/api-key", ensureLoggedInApi("/auth/login"), (req, res, next) => {
  res.render("auth/api-key");
});

router.post("/api-key", ensureLoggedInApi("/auth/login"), async (req, res, next) => {
  if (!req.user) {
    res.status(400).send("No username specified.");
    return;
  }
  if (!req.body.displayname) {
    res.status(400).send("No displayname specified.");
    return;
  }
  const key = `${req.user.username}-${crypto.randomUUID()}`;
  const hash = await bcrypt.hash(key, 10);
  db.prepare(`INSERT INTO api_keys (key, user, displayname) VALUES (?, ?, ?)`)
    .run(hash, req.user.username, req.body.displayname);

  res.status(200).send(key);
});