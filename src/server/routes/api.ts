import express from "express";
import type { Size } from "@photo-sphere-viewer/core";
import { uploadMarker, uploadPanorama } from "../utils/upload.js";
import { ensureLoggedInApi } from "../utils/ensureLoggedInApi.js";
import { db } from "../index.js";

export const router = express.Router();

router.get("/config/:table/:id", (req, res) => {
    let config;
    if (req.params.table === "tours") {
        config = db.getConfigTour(Number(req.params.id));
    } else if (req.params.table === "nodes") {
        config = db.getConfigNode(Number(req.params.id));
    } else {
        res.status(404).send();
        return;
    }

    if (!req.query.nocache) {
        res.setHeader("Cache-Control", "public, max-age=600");
        res.setHeader("Expires", new Date(Date.now() + 600000).toUTCString());
    }

    res.status(200).json(config);
});

router.get("/:table", (req, res) => {
    const tour = req.query.tour;
    const node = req.query.node;
    let query;
    switch (req.params.table) {
        case "tours":
            query = db.prepare(`SELECT * FROM tours WHERE ${tour ? "id = ?" : "TRUE"};`);
            break;
        case "nodes":
            query = db.prepare(`SELECT * FROM nodes WHERE ${tour ? "tour = ?" : "TRUE"};`);
            break;
        case "links":
            // note: only links with node_from in the selected tour will be included
            // (node_to will be ignored)
            if (node) {
                res.status(200).json(db.getDbLinks(Number(node)));
                return;
            } else if (tour) {
                query = db.prepare(
                    `SELECT * FROM links WHERE node_from IN (SELECT id FROM nodes WHERE tour = ?)};`
                );
            } else {
                query = db.prepare(`SELECT * FROM links`);
            }
            break;
        case "markers":
            query = db.prepare(
                `SELECT * FROM markers m WHERE ${tour ? "m.node_id IN (SELECT id FROM nodes n WHERE n.tour = ?)" : "TRUE"};`
            );
            break;
        case "plugins":
            query = db.prepare(
                `SELECT * FROM plugins WHERE ${tour ? "tour = ?" : "TRUE"};`
            );
            break;
        default:
            res.status(404).send("Can't get this table");
            return;
    }
    if (tour) res.status(200).send(query.all(tour));
    else res.status(200).send(query.all());
});

router.get("/:table/:id", (req, res) => {
    let query;
    switch (req.params.table) {
        case "tours":
        case "nodes":
        case "links":
        case "markers":
        case "plugins":
            query = db.prepare(`SELECT * FROM ${req.params.table} where id = ?;`);
            break;
        default:
            res.status(404).send();
            return;
    }
    const row = query.get(req.params.id);
    if (row) res.status(200).send(row);
    else res.status(404).send();
});

router.put("/upload/panorama", ensureLoggedInApi("/auth/login"), async (req, res, next) => {
    if (!req.files) {
        res.status(400).send("No file uploaded.");
        return;
    }
    if (typeof req.body.node !== "string") {
        res.status(400).send("Invalid or missing node.");
        return;
    }
    if (typeof req.body.size !== "string" && req.files.image) {
        res.status(400).send("Invalid or missing size.")
        return;
    }

    if (req.files.image && !(req.files.image instanceof Array)) {
        const size = JSON.parse(req.body.size) as Size;
        if (typeof size.height !== "number" || typeof size.width !== "number") {
            res.status(400).send("Invalid size")
            return;
        }
        try {
            await uploadPanorama(req.files.image, Number(req.body.node), size);
            res.status(204).send();
        } catch (error) {
            next(error);
            return;
        }
    } else if (req.files.video && !(req.files.video instanceof Array)) {
        try {
            await uploadPanorama(req.files.video, Number(req.body.node));
            res.status(204).send();
        } catch (error) {
            next(error);
            return;
        }
    }
    res.status(400).send();
});

router.put("/upload/markers/:id", ensureLoggedInApi("/auth/login"), async (req, res, next) => {
    if (!req.files || !req.files.file || req.files.file instanceof Array) {
        res.status(400).send("No file uploaded or invalid upload usage.");
        return;
    }
    try {
        await uploadMarker(req.files.file, Number(req.params.id));
    } catch (error) {
        next(error);
        return;
    }
    res.status(204).send();
});

router.post("/:table/:tour?", ensureLoggedInApi("/auth/login"), async (req, res) => {
    let id;
    switch (req.params.table) {
        case "tours":
            db.insertTour(req.body);
            res.status(201).send(req.body);
            return;
        case "nodes":
            if (!req.params.tour) {
                res.status(400).send("Nodes need a tour specified.");
                return;
            }
            id = db.insertNode(req.body, Number(req.params.tour));
            break;
        case "links":
            id = db.insertLink(req.body, req.body.node_from);
            break;
        case "markers":
            id = db.insertMarker(req.body.config, req.body.node_id);
            break;
        case "plugins":
            id = db.insertPlugin(req.body[0], req.body[1], Number(req.params.tour));
            break;
        case "users":
            id = await db.insertUser(req.body);
            break;
        case "hotspots":
            id = await db.insertHotspot(req.body);
            break;
        default:
            res.status(404).send("Invalid table");
            return;
    }
    if (id) res.status(201).send({ ...req.body, id: id });
    else res.status(400).send();
});

router.patch("/:table/:id", ensureLoggedInApi("/auth/login"), (req, res) => {
    switch (req.params.table) {
        case "nodes":
        case "links":
        case "markers":
        case "plugins":
        case "tours":
        case "hotspots":
            try {
                res.status(200).send(db.patchRow(req.params.table, Number(req.params.id), req.body));
            } catch (e) {
                res.status(400).send();
            }
            break;
        default:
            res.status(404).send()
            break;
    }
});

router.delete("/:table/:id", ensureLoggedInApi("/auth/login"), async (req, res, next) => {
    switch (req.params.table) {
        case "tours":
            req.session.currentTour = undefined;
        case "nodes":
        case "links":
        case "markers":
        case "plugins":
        case "users":
        case "hotspots":
            try {
                await db.deleteRow(req.params.table, Number(req.params.id))
            } catch (error) {
                next(error);
                return;
            }
            res.status(204).send();
            break;
        default:
            res.status(404).send();
            return;
    }
});