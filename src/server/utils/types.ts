// export type Config = {
//     viewerConfig: string,
//     plugins: string,
//     adapter: string,
//     useCDN: boolean,
// }

export type DBTour = {
    id: number,
    name: string
    type: string,
    viewerConfig: string,
    adapter: string,
    useCDN: number,
    introAnimation: number,
};

export type DBNode = {
    id: number,
    caption: string | null,
    data: string | null,
    description: string | null,
    gps: string,
    map: string | null,
    name: string | null,
    panoData: string | null,
    panorama: string,
    plan: string | null,
    sphereCorrection: string | null,
    thumbnail: string | null,
    tour: string,
};

export type DBLink = {
    id: number,
    node_from: number,
    node_to: number,
    arrowStyle: string | null,
    data: string | null,
    linkOffset: string | null,
    markerStyle: string | null,
};

export type DBMarker = {
    id: number,
    node_id: number,
    config: string,
};

export type DBPlugin = {
    id: number,
    name: string,
    config: string,
    tour: number,
}

export type DBUser = {
    username: string,
    displayname: string | null,
    password: string,
    last_login: number | null,
    role: string | null,
}

export type DBHotspot = {
    id: number,
    node: number,
    size: number,
    color: string,
    hoverSize: number | null,
    hoverColor: string | null,
    hoverBorderSize: number,
    hoverBorderColor: string,
    tooltip: string | null,
}