import fileUpload from "express-fileupload";
import type { Size } from "@photo-sphere-viewer/core";
import { ASSETS_DIR, PANORAMAS_DIR } from "../constants/paths.js";
import * as fs from "fs/promises";
import gm from "gm";
import { db } from "../index.js";
import { DBMarker } from "./types.js";
import { ParsedMarkerConfig } from "@photo-sphere-viewer/markers-plugin";
import ffmpeg from "fluent-ffmpeg";


// TODO: allow other than .jpg
export async function uploadPanorama(file: fileUpload.UploadedFile, node: number | bigint, size?: Size) {
    // TODO: calculate size server side via gm 
    const tour = db.prepare(`SELECT t.type, n.tour FROM nodes n JOIN tours t ON n.tour = t.id WHERE n.id = ?;`).get(node);

    if (!tour || typeof tour !== "object" || !("type" in tour) || !("tour" in tour)) {
        const err = new Error("Node not found.") as any;
        err.statusCode = 404;
        throw err;
    }
    // if (tour.type === "equirectangular") {
    //     await file.mv(`${PANORAMAS_DIR}/${tour.tour}/${node}.jpg`);

    // } else
    if (tour.type === "equirectangular-tiles" || tour.type === "equirectangular") {
        await fs.mkdir(`${PANORAMAS_DIR}/${node}`, { recursive: true });
        await file.mv(`${PANORAMAS_DIR}/${node}/full.jpg`);

        const tileHeight = Math.ceil(size!.height / 8);
        const tileWidth = Math.ceil(size!.width / 16);

        await Promise.all([
            new Promise<void>((resolve, reject) => {
                gm(`${PANORAMAS_DIR}/${node}/full.jpg`)
                    .out("-crop")
                    .out(`${tileWidth}x${tileHeight}`)
                    .quality(95)
                    .out("+adjoin")
                    .write(`${PANORAMAS_DIR}/${node}/tile%d.jpg`, err => {
                        if (err) reject(err);
                        else resolve();
                    });
            }),
            new Promise<void>((resolve, reject) => {
                gm(`${PANORAMAS_DIR}/${node}/full.jpg`)
                    .resize(200 * size!.width / size!.height, 200)
                    .quality(95)
                    .write(`${PANORAMAS_DIR}/${node}/base.jpg`, err => {
                        if (err) reject(err);
                        else resolve();
                    });
            })
        ]);
    } else if (tour.type === "equirectangular-video") {
        await fs.mkdir(`${PANORAMAS_DIR}/${node}`, { recursive: true });
        await file.mv(`${PANORAMAS_DIR}/${node}/video.mp4`);
    }

    if (tour.type === "equirectangular-tiles") {
        const panorama = JSON.parse((
            db.prepare(`SELECT panorama FROM nodes WHERE id = ?`).get(node) as { panorama: string }
        ).panorama);
        panorama.width = size!.width;
        db.prepare(`UPDATE nodes SET panorama = ? WHERE id = ?`).run(JSON.stringify(panorama), node);
    }

}

export async function uploadMarker(file: fileUpload.UploadedFile, markerId: number | bigint) {
    const marker = db.prepare(`SELECT * FROM markers WHERE id = ?`).get(markerId) as DBMarker | undefined;
    if (!marker) {
        const err = new Error("Marker not found.") as any;
        err.statusCode = 404;
        throw err;
    }

    const config = JSON.parse(marker.config) as ParsedMarkerConfig;

    await fs.mkdir(`${ASSETS_DIR}/markers`, { recursive: true });
    if (config.image || config.imageLayer) {
        await new Promise<void>((resolve, reject) => {
            gm(file.tempFilePath)
                .write(`${ASSETS_DIR}/markers/${marker.id}.png`, err => {
                    if (err) reject(err);
                    else resolve();
                });
        });
    } else if (config.videoLayer) {
        await new Promise<void>((resolve, reject) => {
            ffmpeg(file.tempFilePath, { timeout: 600 })
                .outputOptions("-c:v", "libx264", "-b:v", "2M", "-maxrate", "2M", "-bufsize", "3M", "-c:a", "libmp3lame", "-movflags", "+faststart", "-format", "mp4")
                .saveToFile(`${ASSETS_DIR}/markers/${marker.id}.mp4`)
                .on("error", (err) => {
                    reject(err);
                })
                .on("end", () => {
                    resolve();
                });
        })
    }
}