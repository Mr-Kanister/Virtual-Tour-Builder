export function JSONParse<R>(string: string | undefined | null): R | undefined {
    if (typeof string === "string")
        return JSON.parse(string);
    else
        return undefined;
}