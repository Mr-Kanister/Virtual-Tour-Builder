import { ensureLoggedIn, LoggedInOptions } from "connect-ensure-login";
import { NextFunction, Request, Response } from "express";
import passport from "passport";

export function ensureLoggedInApi(options?: LoggedInOptions | string) {
  return (req: Request, res: Response, next: NextFunction) => {
    if (req.query.access_token || req.body.access_token ||
      "authorization" in req.headers
    ) {
      passport.authenticate("bearer", { session: false })(req, res, next);
    } else {
      ensureLoggedIn(options)(req, res, next);
    }
  }
}