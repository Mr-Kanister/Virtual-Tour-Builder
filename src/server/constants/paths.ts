export const ASSETS_DIR = "../public/assets";
export const RESOURCES_DIR = "../public/resources";
export const PANORAMAS_DIR = `${ASSETS_DIR}/panoramas`;