import { Database as DB } from "better-sqlite3";
import {runMigration} from "./migration/runMigration.js";

export function initDb(db: DB) {
    db.pragma("journal_mode = WAL");

    const firstStart = db.prepare("SELECT name FROM sqlite_schema WHERE type='table' AND name='users';").get() === undefined;
    db.prepare(`
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username TEXT UNIQUE NOT NULL,
            displayname TEXT,
            password TEXT NOT NULL,
            last_login INTEGER,
            role TEXT
        );`).run();
    if (firstStart) {
        db.prepare("INSERT INTO users (username, password, displayname) VALUES (?, ?, ?)").run(
            "admin",
            "$2a$10$LrUyXW5EZqkx55dYNV/uwefFIL0x/dlEvpWnBvIBpCuix633dGKE.",
            "Admin",
        );
    }

    [
        db.prepare(`
            CREATE TABLE IF NOT EXISTS tours (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT NOT NULL,
                type TEXT NOT NULL ON CONFLICT REPLACE DEFAULT 'equirectangular',
                viewerConfig TEXT NOT NULL ON CONFLICT REPLACE DEFAULT '{}',
                adapter TEXT NOT NULL ON CONFLICT REPLACE DEFAULT '["equirectangular",{}]',
                useCDN INTEGER NOT NULL ON CONFLICT REPLACE DEFAULT 0,
                introAnimation INTEGER DEFAULT 0,
                CHECK (type IN ('equirectangular', 'equirectangular-tiles', 'equirectangular-video'))
            );`),
        db.prepare(`
            CREATE TABLE IF NOT EXISTS nodes (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                caption TEXT,
                data TEXT,
                description TEXT,
                gps TEXT NOT NULL ON CONFLICT REPLACE DEFAULT '[11, 49]',
                map TEXT,
                name TEXT,
                panoData TEXT,
                panorama TEXT NOT NULL,
                plan TEXT,
                sphereCorrection TEXT,
                thumbnail TEXT,
                tour INTEGER NOT NULL,
                CONSTRAINT fk_tour
                    FOREIGN KEY(tour)
                    REFERENCES tours(id)
                    ON DELETE CASCADE
            );`),
        db.prepare(`
            CREATE TABLE IF NOT EXISTS links (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                node_from INTEGER NOT NULL,
                node_to INTEGER NOT NULL,
                arrowStyle TEXT,
                data TEXT,
                linkOffset TEXT,
                markerStyle TEXT,
                CONSTRAINT fk_node_from
                    FOREIGN KEY(node_from)
                    REFERENCES nodes(id)
                    ON DELETE CASCADE
                CONSTRAINT fk_node_to
                    FOREIGN KEY(node_to)
                    REFERENCES nodes(id)
                    ON DELETE CASCADE
                );`),
        db.prepare(`
            CREATE TABLE IF NOT EXISTS markers (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                node_id INTEGER NOT NULL,
                config TEXT NOT NULL,
                CONSTRAINT fk_node_id
                    FOREIGN KEY(node_id)
                    REFERENCES nodes(id)
                    ON DELETE CASCADE
            );`),
        db.prepare(`
            CREATE TABLE IF NOT EXISTS api_keys (
                key TEXT PRIMARY KEY NOT NULL,
                user TEXT NOT NULL,
                displayname TEXT NOT NULL,
                CONSTRAINT fk_user
                    FOREIGN KEY(user)
                    REFERENCES users(username)
                    ON DELETE CASCADE
            );`),
        db.prepare(`
            CREATE TABLE IF NOT EXISTS plugins (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT NOT NULL,
                config TEXT NOT NULL,
                tour INTEGER NOT NULL,
                CONSTRAINT fk_tour
                    FOREIGN KEY(tour)
                    REFERENCES tours(id)
                    ON DELETE CASCADE
            );`),
        db.prepare(`
            CREATE TABLE IF NOT EXISTS hotspots (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                node INTEGER NOT NULL,
                size INTEGER NOT NULL ON CONFLICT REPLACE DEFAULT 15,
                color TEXT NOT NULL ON CONFLICT REPLACE DEFAULT 'white',
                hoverSize INTEGER,
                hoverColor TEXT,
                hoverBorderSize INTEGER NOT NULL ON CONFLICT REPLACE DEFAULT 4,
                hoverBorderColor TEXT NOT NULL ON CONFLICT REPLACE DEFAULT 'rgba(255,255,255,0.8)',
                tooltip TEXT,
                coordinates TEXT NOT NULL,
                CONSTRAINT fk_node
                    FOREIGN KEY(node)
                    REFERENCES nodes(id)
                    ON DELETE CASCADE
            );`)
    ].forEach(x => x.run());

    runMigration(db);
}