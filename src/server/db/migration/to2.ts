import { Database as DB } from "better-sqlite3";

export function migrateTo2(db: DB) {
    // drop coordinates of hotspots
    db.prepare("ALTER TABLE hotspots DROP COLUMN coordinates;").run();

    // update user_version
    db.pragma("user_version = 2");
}