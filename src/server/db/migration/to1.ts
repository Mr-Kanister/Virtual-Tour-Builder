import { Database as DB } from "better-sqlite3";

export function migrateTo1(db: DB) {
    // switch node gps default value from [49,11] to [11,49]
    db.prepare("ALTER TABLE nodes RENAME COLUMN gps TO gps_old;").run();
    db.prepare("ALTER TABLE nodes ADD COLUMN gps TEXT NOT NULL ON CONFLICT REPLACE DEFAULT '[11, 49]';").run();
    db.prepare("UPDATE nodes SET gps = gps_old;").run();
    db.prepare("ALTER TABLE nodes DROP COLUMN gps_old;").run();

    // hotspot table will be added in initialization of db

    // transfer hotspots from nodes to new table
    const nodes = db.prepare("SELECT id, data, plan, gps FROM nodes").all() as
        { id: number, data: string | null, plan: string | null, gps: string }[];

    const insertHotspot = db.prepare(`
        INSERT INTO hotspots
        (node, size, color, hoverSize, hoverColor, hoverBorderSize, hoverBorderColor, coordinates)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?);`);
    const updateNodeData = db.prepare("UPDATE nodes SET data = ? WHERE id = ?;");
    const removeNodePlan = db.prepare("UPDATE nodes SET plan = NULL WHERE id = ?;");

    nodes.forEach(node => {
        const data = JSON.parse(node.data ?? "{}");
        const plan = JSON.parse(node.plan ?? "{}");
        if (!data.hotspot) return;

        insertHotspot.run(
            node.id,
            plan?.size ?? null,
            plan?.color ?? null,
            plan?.hoverSize ?? null,
            plan?.hoverColor ?? null,
            plan?.hoverBorderSize ?? null,
            plan?.hoverBorderColor ?? null,
            node.gps,
        );

        data.hotspot = undefined;
        updateNodeData.run(JSON.stringify(data), node.id);
        removeNodePlan.run(node.id);
    });

    // update user_version
    db.pragma("user_version = 1");
}