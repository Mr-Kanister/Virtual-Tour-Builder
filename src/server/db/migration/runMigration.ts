import { Database as DB } from "better-sqlite3";
import { migrateTo1 } from "./to1.js";
import { migrateTo2 } from "./to2.js";

export function runMigration(db: DB) {
    const fromVersion = db.pragma("user_version", { simple: true }) as number;

    switch (fromVersion + 1) {
        // - specify what changes are done in the version
        // - always do changes inside a trasaction
        // - don't break after the changes so that all changes until the most
        //   recent version are applied
        case 1:
            db.transaction(migrateTo1)(db);
        case 2:
            db.transaction(migrateTo2)(db);
    }
}