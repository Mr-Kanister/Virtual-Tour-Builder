import Database, { Database as DB } from "better-sqlite3";
import type { VirtualTourLink, VirtualTourNode, VirtualTourPluginConfig } from "@photo-sphere-viewer/virtual-tour-plugin";
import type { MarkerConfig } from "@photo-sphere-viewer/markers-plugin";
import * as fs from "fs/promises";
import { ASSETS_DIR, PANORAMAS_DIR } from "../constants/paths.js";
import { Config, PluginConfig, VirtualTourLinkData } from "virtual-tour-viewer/dist/module/types.js";
import { DBHotspot, DBLink, DBMarker, DBNode, DBTour, DBUser } from "../utils/types.js";
import bcrypt from "bcryptjs";
import { initDb } from "./init.js";
import { JSONParse } from "../utils/json.js";
import { PlanHotspot } from "@photo-sphere-viewer/plan-plugin";

export class VTBDB {
    private db: DB;

    constructor(path = "./database.db") {
        const db = new Database(path, {/*verbose: console.log*/ });
        initDb(db);
        this.db = db;
    }

    public getDB() {
        return this.db;
    }

    public prepare(query: string) {
        return this.db.prepare(query);
    }

    // -------
    // Plugins
    // -------
    public insertPlugin(name: string, config: PluginConfig, tourId: number | bigint) {
        const query = this.db.prepare(`
            INSERT INTO plugins (name, config, tour) VALUES (
                @name,
                json(@config),
                @tour
            );`);
        try {
            const { lastInsertRowid: id } = query.run({
                name, config: JSON.stringify(config), tour: tourId
            });
            return id
        } catch (error: any) {
            if (error.code?.includes("SQLITE_CONSTRAINT"))
                error.statusCode = 400;
            throw error;
        }
    }

    // -------
    // Hotspots
    // -------
    public async insertHotspot(hotspot: Omit<DBHotspot, "id">) {
        const emptyHotspot = {
            size: null,
            color: null,
            hoverSize: null,
            hoverColor: null,
            hoverBorderSize: null,
            hoverBorderColor: null,
            tooltip: null,
        };
        const query = this.db.prepare(`
            INSERT INTO hotspots (node, size, hoverSize, hoverColor, hoverBorderSize, hoverBorderColor, tooltip) VALUES (
                @node,
                @size,
                @hoverSize,
                @hoverColor,
                @hoverBorderSize,
                @hoverBorderColor,
                @tooltip
            );`);
        try {
            const { lastInsertRowid: id } = query.run({ ...emptyHotspot, ...hotspot });
            return id;
        } catch (error: any) {
            if (error.code?.includes("SQLITE_CONSTRAINT")) {
                error.statusCode = 400;
            }
            throw error;
        }
    }

    public getDbHotspot(nodeId: number): DBHotspot | undefined {
        return this.db.prepare<number, DBHotspot>(`SELECT * FROM hotspots WHERE node = ?`).get(nodeId);
    }

    public getDbHotspots(entryTour: number): (DBHotspot & {
        gps: string,
        tour: number,
        name: string | null
    })[] {
        return this.db.prepare(`
            -- get all tour relations (tour 1 links to tour 2, 2 to 3, 1 to 1, etc.)
            WITH RECURSIVE tour_relations AS
            (
                SELECT n1.tour AS tour_from, n2.tour AS tour_to
                FROM links 
                JOIN nodes n1
                ON links.node_from = n1.id
                JOIN nodes n2
                ON links.node_to = n2.id
            ),

            -- get all tour relation chains (tour 1 -> 2 -> 3, 1 -> 1, etc.)
            relation_chains AS
            (
                SELECT tour_from AS start, tour_from, tour_to
                FROM tour_relations

                UNION

                SELECT c.start, r.tour_from, r.tour_to
                FROM relation_chains c
                JOIN tour_relations r ON r.tour_from = c.tour_to
            )

            -- get hotspots of all tours accessible from the given entry tour
            SELECT hotspots.*, nodes.gps, nodes.tour, nodes.name
            FROM hotspots
            JOIN nodes
            ON nodes.id = hotspots.node
            WHERE nodes.tour IN (
                SELECT DISTINCT tour_to
                FROM relation_chains
                WHERE start = @entryTour
                UNION
                SELECT @entryTour AS tour_to
            )`
        ).all({ entryTour }) as (DBHotspot & {
            gps: string,
            tour: number,
            name: string | null
        })[];
    }

    public getVirtualTourHotspots(entryTour: number): (PlanHotspot & { data?: { loadTourConfigUrl?: string } })[] {
        const hotspots = this.getDbHotspots(entryTour);
        return hotspots.map(hotspot => {
            let id = `__tour-hotspot__${hotspot.node}`;
            if (hotspot.tour !== entryTour)
                id = `__cross-hotspot__${hotspot.node}`;

            let tooltip: { content: string } | undefined = JSONParse(hotspot.tooltip);
            tooltip ??= { content: hotspot.name ?? "" };

            return {
                id,
                coordinates: JSONParse(hotspot.gps)!,
                size: hotspot.size,
                color: hotspot.color,
                hoverSize: hotspot.hoverSize ?? undefined,
                hoverColor: hotspot.hoverColor ?? undefined,
                hoverBorderSize: hotspot.hoverBorderSize,
                hoverBorderColor: hotspot.hoverBorderColor,
                tooltip: tooltip,
                data: hotspot.tour !== entryTour ?
                    { loadTourConfigUrl: `/api/config/tours/${hotspot.tour}` } :
                    undefined,
            };
        });
    }

    // -------
    // Tours
    // -------
    public insertTour(body: { name: string, type: string }) {
        try {
            this.db.transaction(() => {
                const defaultViewerConfig = {
                    navbar: [
                        ...body.type === "equirectangular-video" ? [
                            "videoPlay",
                            "videoVolume",
                            "videoTime",
                        ] : [
                            "zoomOut",
                            "zoomRange",
                            "zoomIn",
                        ],
                        "description",
                        "caption",
                        "fullscreen",
                        "markersList",
                    ]
                };
                const { lastInsertRowid: id } = this.db.prepare(`
                    INSERT
                    INTO tours (name, type, adapter, viewerConfig)
                    VALUES (?, ?, json(?), json(?));`)
                    .run(
                        body.name,
                        body.type,
                        `["${body.type}", {}]`,
                        JSON.stringify(defaultViewerConfig),
                    );
                this.insertPlugin("virtual-tour", { positionMode: "gps" }, id);
                this.insertPlugin("markers", {}, id);

                if (body.type === "equirectangular-video") {
                    this.insertPlugin("video", {}, id);
                }
            })();
        } catch (error: any) {
            if (error.code?.includes("SQLITE_CONSTRAINT")) {
                error.statusCode = 400;
            }
            throw error;
        }
    }

    /**
     * Filters all tours for the given parameters. If none is given, returns one of all tours.
     * @param tourName
     * @param nodeId
     * @returns the found tour
     */
    public getDbTour(options: { tourId?: number, nodeId?: number }): DBTour {
        const tour = this.db.prepare(`
            SELECT t.id, t.name, t.type, t.viewerConfig, t.adapter, t.useCDN, t.introAnimation
            FROM tours t
            ${options.nodeId ? "JOIN nodes n ON t.id = n.tour" : ""}
            WHERE ${options.tourId ? "t.id = @tourId" : "TRUE"}
            AND
            ${options.nodeId ? "n.id = @nodeId" : "TRUE"}
        `).get(options) as DBTour | undefined;

        if (!tour) {
            const err = new Error("Tour not found.") as any;
            err.statusCode = 404;
            throw err;
        }
        return tour;
    }

    /**
     * Gives all tours
     * @returns all tours
     */
    public getDbTours(): DBTour[] {
        return this.db.prepare(`SELECT * FROM tours`).all() as DBTour[];
    }

    // -------
    // Links
    // -------
    public insertLink(link: VirtualTourLink, node_from: number | bigint) {
        const emptyLink = {
            node_from: node_from,
            node_to: null,
            arrowStyle: null,
            data: null,
            linkOffset: null,
        };
        const query = this.db.prepare(`
            INSERT INTO links (node_from, node_to, arrowStyle, data, linkOffset) VALUES (
                @node_from,
                @node_to,
                json(@arrowStyle),
                json(@data),
                json(@linkOffset)
            );`);
        try {
            const { lastInsertRowid: id } = query.run({ ...emptyLink, ...link });
            return id;
        } catch (error: any) {
            if (error.code?.includes("SQLITE_CONSTRAINT")) {
                error.statusCode = 400;
            }
            throw error;
        }
    }

    public getDbLinks(nodeId: number): (DBLink & Pick<DBNode, "name" | "gps">)[] {
        const links = this.db.prepare(`
            SELECT l.id, node_from, node_to, arrowStyle, l.data, linkOffset, markerStyle, gps, name
            FROM links l
            LEFT JOIN nodes n
            ON l.node_to = n.id
            WHERE node_from = ?
        `).all(nodeId) as (DBLink & Pick<DBNode, "name" | "gps">)[];

        // add the real node name and gps on cross tour links
        links.forEach(link => {
            if (link.node_to !== -1 || !link.data) return;
            const data = JSONParse<VirtualTourLinkData>(link.data);
            const toNodeId = data?.loadTour?.startNodeId;
            if (!toNodeId) return;
            const node = this.getDbNode(Number(toNodeId));
            link.name = node.name ?? "Cross link";
            link.gps = node.gps;
        });

        return links;
    }

    public getVirtualTourLinks(nodeId: number): VirtualTourLink[] {
        const links = this.getDbLinks(nodeId);
        return links.map(link => {
            return {
                nodeId: link.node_to.toString(),
                arrowStyle: JSONParse(link.arrowStyle),
                data: {
                    tooltip: link.name ?? undefined,
                    ...JSONParse<VirtualTourLinkData>(link.data),
                },
                gps: JSONParse(link.gps),
                linkOffset: JSONParse(link.linkOffset),
            };
        });
    }

    // -------
    // Chains
    // -------
    public getChains(nodeId: number) {
        const chains = this.db.prepare(`
            SELECT
                chain.key AS fromId,
                chain.value -> '$.to' AS toId,
                n2.name as fromName,
                n3.name as toName
            FROM nodes, json_each(nodes.data, '$.chains') AS chain
            LEFT JOIN nodes n2 ON chain.key = n2.id
            LEFT JOIN nodes n3 ON chain.value = n3.id
            WHERE nodes.id = ?;
        `).all(nodeId) as {
            fromId: number,
            toId: number,
            fromName: string | null,
            toName: string | null,
        }[];

        return chains;
    }

    // -------
    // Markers
    // -------
    public insertMarker(marker: MarkerConfig, node_id: number | bigint) {
        const query = this.db.prepare(`
            INSERT INTO markers (node_id, config) VALUES (
                @node_id,
                json(@config)
            );`);
        try {
            const { lastInsertRowid: id } = query.run({ node_id: node_id, config: marker });
            return id
        } catch (error: any) {
            if (error.code?.includes("SQLITE_CONSTRAINT"))
                error.statusCode = 400;
            throw error;
        }
    }

    public getDbMarkers(nodeId: number): DBMarker[] {
        return this.db.prepare(`SELECT * FROM markers WHERE node_id = ?`).all(nodeId) as DBMarker[];
    }

    public getVirtualTourMarkers(nodeId: number): MarkerConfig[] {
        const dbMakers = this.getDbMarkers(nodeId);
        return dbMakers.map(marker => {
            return {
                ...JSON.parse(marker.config),
                id: marker.id,
            } as MarkerConfig;
        });
    }

    // -------
    // Nodes
    // -------
    private dbNodeToVirtualTourNode(dbNode: DBNode): VirtualTourNode {
        const node: VirtualTourNode = {
            id: dbNode.id.toString(),
            panorama: dbNode.panorama,
            caption: dbNode.caption ?? undefined,
            data: JSONParse(dbNode.data),
            description: dbNode.description ?? undefined,
            gps: JSONParse(dbNode.gps),
            links: this.getVirtualTourLinks(dbNode.id),
            map: JSONParse(dbNode.map),
            markers: this.getVirtualTourMarkers(dbNode.id),
            name: dbNode.name ?? undefined,
            panoData: JSONParse(dbNode.panoData),
            plan: JSONParse(dbNode.plan),
            sphereCorrection: JSONParse(dbNode.sphereCorrection),
            thumbnail: dbNode.thumbnail ?? undefined,
        };
        return node;
    }

    public insertNode(node: VirtualTourNode, tourId: number) {
        const tour = this.getDbTour({ tourId });

        const insertNodeTransaction = this.db.transaction(() => {
            const emptyNode = {
                caption: null,
                data: null,
                description: null,
                gps: null,
                map: null,
                name: null,
                panoData: null,
                plan: null,
                sphereCorrection: null,
                thumbnail: null,
                tour: null,
                panorama: "",
            };
            const { lastInsertRowid: id } = this.db.prepare(`
                INSERT INTO nodes (caption, data, description, gps, map, name, panoData, panorama, plan, sphereCorrection, thumbnail, tour) VALUES (
                    @caption,
                    json(@data),
                    @description,
                    json(@gps),
                    json(@map),
                    @name,
                    json(@panoData),
                    @panorama,
                    json(@plan),
                    json(@sphereCorrection),
                    @thumbnail,
                    @tour
                );`).run({ ...emptyNode, ...node, tour: tourId });

            const setPanorama = this.db.prepare(`UPDATE nodes SET panorama = ? WHERE id = ?`);
            // TODO make relative to rootDir
            // TODO check if virtual tour plugin is used
            if (tour.type === "equirectangular") {
                setPanorama.run(`/assets/panoramas/${id}/full.jpg`, id);
            } else if (tour.type === "equirectangular-tiles") {
                setPanorama.run(`{"cols": 16, "rows": 8}`, id);
            } else if (tour.type === "equirectangular-video") {
                setPanorama.run(`{"source": "/assets/panoramas/${id}/video.mp4"}`, id);
            }
            return id;
        });

        try {
            const id = insertNodeTransaction();
            node.links?.forEach(link => this.insertLink(link, id));
            node.markers?.forEach(marker => this.insertMarker(marker, id));
            return id;
        } catch (error: any) {
            if (error.code?.includes("SQLITE_CONSTRAINT")) {
                error.statusCode = 400;
            }
            throw error;
        }
    }

    public getDbNode(nodeId: number): DBNode {
        return this.db.prepare(`SELECT * FROM nodes WHERE id = ?`).get(nodeId) as DBNode;
    }

    public getVirtualTourNode(nodeId: number): VirtualTourNode {
        return this.dbNodeToVirtualTourNode(this.getDbNode(nodeId));
    }

    public getDbNodes(tourId?: number): DBNode[] {
        if (tourId !== undefined) {
            return this.db.prepare(`SELECT * FROM nodes WHERE tour = ?;`)
                .all(tourId) as DBNode[];
        } else {
            return this.db.prepare(`SELECT * FROM nodes;`)
                .all() as DBNode[];
        }
    }

    public getVirtualTourNodes(tourId?: number): VirtualTourNode[] {
        return this.getDbNodes(tourId).map(n => this.dbNodeToVirtualTourNode(n));
    }

    // -------
    // Users
    // -------
    public async insertUser(user: {
        username: string,
        displayname?: string,
        password: string,
        role?: string
    }) {

        // hash password
        const hash = await bcrypt.hash(user.password, 10);

        const query = this.db.prepare(`INSERT INTO users (username, displayname, password, role) VALUES (
            @username,
            @displayname,
            @password,
            @role
        )`);

        try {
            const { lastInsertRowid: id } = query.run({
                displayname: null,
                role: null,
                ...user,
                password: hash
            });
            return id;
        } catch (error: any) {
            if (error.code?.includes("SQLITE_CONSTRAINT")) {
                error.statusCode = 400;
            }
            throw error;
        }
    }

    public getUsers() {
        return this.db.prepare(`SELECT * FROM users`).all() as DBUser[];
    }


    // -------
    // Configs
    // -------
    public getConfigTour(tourId: number): Config {
        const tour = this.getDbTour({ tourId });

        const plugins = (this.db.prepare(`
            SELECT name, config
            FROM plugins
            WHERE tour = ?;
        `).all(tourId) as { name: string, config: string }[])
            .map(plugin => [plugin.name, JSON.parse(plugin.config)] as [string, PluginConfig]);

        const virtualTourPlugin = plugins.find(p => p[0] === "virtual-tour");

        if (virtualTourPlugin) {
            const config = virtualTourPlugin[1] as VirtualTourPluginConfig;

            config.nodes = this.getVirtualTourNodes(tourId);
            config.nodes.forEach(node => node.links?.forEach(link => {
                // check if link is cross tour link
                const crossLink = config.nodes?.find(n => n.id === link.nodeId) === undefined;
                if (!crossLink) return;

                // setup cross tour link
                const crossNode = crossLink ? this.getDbNode(Number(link.nodeId)) : undefined;
                if (!crossNode) return;

                link.data = {
                    ...link.data,
                    loadTour: {
                        configUrl: `/api/config/tours/${crossNode.tour}`,
                        startNodeId: link.nodeId,
                    },
                };
                link.gps = JSON.parse(crossNode.gps);
                link.nodeId = "-1";
            }));

            // insert dummy node to make cross linking between tours possible
            let dummyPanorama: any = undefined;
            switch (tour.type) {
                case "equirectangular":
                    dummyPanorama = "/assets/panoramas/-1/full.jpg";
                    break;
                case "equirectangular-tiles":
                    dummyPanorama = `{"cols": 16, "rows": 8, "width": 5760}`;
                    break;
                case "equirectangular-video":
                    dummyPanorama = `{"source": "/assets/panoramas/-1/video.mp4"}`;
                    break;
            }
            if (dummyPanorama) {
                config.nodes.push({
                    id: "-1",
                    panorama: dummyPanorama,
                    gps: [0, 0],
                });
            }
        }

        return {
            ...tour,
            useCDN: tour.useCDN === 1,
            introAnimation: tour.introAnimation === 1,
            viewerConfig: JSON.parse(tour.viewerConfig),
            adapter: JSON.parse(tour.adapter),
            plugins,
            hotspots: this.getVirtualTourHotspots(tourId),
        };
    }

    public getConfigNode(nodeId: number): Config {
        const node = this.getVirtualTourNode(nodeId);
        const tour = this.getDbTour({ nodeId });
        const plugins: [string, PluginConfig][] = [[
            "markers", {
                ...JSON.parse((this.db.prepare(
                    `SELECT config FROM plugins WHERE tour = ? AND name = 'markers'`
                ).get(tour.id) as { config: string }).config),
                markers: this.getDbMarkers(nodeId).map(m => {
                    const config = JSON.parse(m.config) as MarkerConfig;
                    config.id = m.id.toString();
                    return config;
                }),
            },
        ]];

        if (node.gps) {
            plugins.push([
                "plan", {
                    coordinates: node.gps,
                },
            ]);
        }

        if (tour.type === "equirectangular-video") {
            plugins.push(["video", {}]);
        }

        return {
            viewerConfig: {
                description: node.description,
                caption: node.caption,
                panorama: tour.type === "equirectangular-video" ?
                    { source: `/assets/panoramas/${node.id}/video.mp4` } :
                    `/assets/panoramas/${node.id}/full.jpg`,
                sphereCorrection: node.sphereCorrection,
            },
            adapter: tour.type === "equirectangular-video" ? ["equirectangular-video", {}] : ["equirectangular", {}],
            plugins,
            useCDN: false,
            hotspots: this.getVirtualTourHotspots(tour.id),
        };
    }

    // -------
    // Table modifying
    // -------
    public patchRow(table: string, id: number | bigint, changes: object) {
        // TODO: fix SQLI
        // Loop through all proposed changes, check if they are
        // allowed and commit afterwards
        this.db.transaction(() => {
            for (const [key, val] of Object.entries(changes)) {
                if (key === "id") continue;
                // TODO: support tour changing
                // if (key === "tour") {
                //     const oldType = db.prepare(`SELECT t.type FROM nodes n JOIN tours t ON n.tour = t.id WHERE n.id = ?`).get(node);
                //     const newType = db.prepare(`SELECT type FROM tours WHERE id = ?`).run(val);
                //     if (oldType !== newType) throw userError;
                // }
                try {
                    this.db.prepare(`UPDATE ${table} SET ${key} = @val WHERE id = @id;`).run({ val, id });
                } catch (error: any) {
                    if (error.code?.includes("SQLITE_CONSTRAINT"))
                        error.statusCode = 400;
                    throw error;
                }
            }
        })();

        return this.db.prepare(`SELECT * FROM ${table} where id = ?;`).get(id);
    }

    public async deleteRow(table: string, id: number) {
        // TODO: transaction?
        if (table === "nodes") {
            // delete all pictures of the node
            const tour = this.getDbTour({ nodeId: id });

            // if (tour.type === "equirectangular") {
            //     await fs.rm(`${PANORAMAS_DIR}/${tour.id}/${id}.jpg`, { force: true });
            // } else
            if (tour.type === "equirectangular-tiles" || tour.type === "equirectangular") {
                await fs.rm(`${PANORAMAS_DIR}/${id}`, { recursive: true, force: true });
            } else if (tour.type === "equirectangular-video") {
                await fs.rm(`${PANORAMAS_DIR}/${id}/video.mp4`, { force: true });
            }
        }

        if (table === "tours") {
            // delete all nodes and pictures of the tour
            const pendingNodes = this.getVirtualTourNodes(id);
            await Promise.all(pendingNodes.map(async node => this.deleteRow("nodes", Number(node.id))));
        }

        if (table === "markers") {
            // delete eventually uploaded file
            await fs.rm(`${ASSETS_DIR}/markers/${id}.png`, { force: true });
            await fs.rm(`${ASSETS_DIR}/markers/${id}.mp4`, { force: true });
        }

        this.db.prepare(`DELETE FROM ${table} WHERE id = ?;`).run(id);
    }
}