import { VTBDB } from "./db/VTBDB.js";
import express from "express";
import fileUpload from "express-fileupload";
import passport from "passport";
import { router as authRouter } from "./routes/auth.js";
import { router as apiRouter } from "./routes/api.js";
import { router as appRouter } from "./routes/app.js";
import session from "express-session";
import SQLiteStore from "better-sqlite3-session-store";
import expressEjsLayouts from "express-ejs-layouts";
import compression from "compression";
import i18next from "i18next";
import * as i18nextMiddleware from "i18next-http-middleware";
import FsBackend, { FsBackendOptions } from "i18next-fs-backend";

if (!process.env.APP_PORT) {
  console.error("Environment variable APP_PORT not set.");
  process.exit(1);
}

const app = express();
export const db = new VTBDB();

declare module "express-session" {
  interface SessionData {
    currentTour: number;
  }
}
i18next.use(FsBackend);
i18next.use(i18nextMiddleware.LanguageDetector);
i18next.init<FsBackendOptions>({
  fallbackLng: "en",
  preload: ["en, de"],
  ns: ["common", "help", "tours", "button", "nodes", "markers", "user"],
  backend: {
    loadPath: "locales/{{lng}}/{{ns}}.json",
    addPath: "locales/{{lng}}/{{ns}}.missing.json",
  },
  saveMissing: true,
});

app.use(i18nextMiddleware.handle(i18next, {
  ignoreRoutes: ["/api"],
}));

app.set("views", "./views");
app.set("view engine", "ejs");

app.use("/assets", compression(), express.static("../public/assets"));
app.use("/resources", compression(), express.static("../public/resources"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(expressEjsLayouts);
app.set("layout", "components/layout.ejs");
app.set("layout extractScripts", true)

app.use(session({
  secret: "keyboard cat", // TODO
  resave: false,
  saveUninitialized: false,
  store: new (SQLiteStore(session))({
    client: db.getDB(),
    expired: {
      clear: true,
      intervalMs: 3600000 // 60 min
    }
  })
}));
app.use(passport.authenticate("session"));
app.use((req, res, next) => {
  const userId = req.user?.id;

  if (userId) {
    db.patchRow("users", userId, { last_login: new Date().getTime() });
  }

  next();
});

app.use(fileUpload({
  createParentPath: true,
  safeFileNames: true,
  useTempFiles: true,
}));

app.use("/auth", authRouter);
app.use("/api", apiRouter);
app.use("/app", appRouter);
app.get("/", (req, res) => {
  res.setHeader("Cache-Control", "public, max-age=600");
  res.setHeader("Expires", new Date(Date.now() + 600000).toUTCString());
  res.render("index.ejs", { layout: false });
});
app.listen(process.env.APP_PORT);