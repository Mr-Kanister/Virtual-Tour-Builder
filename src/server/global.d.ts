export { };

declare global {
    namespace Express {
        interface User {
            id?: number,
            first_name?: string;
            last_name?: string;
            username: string;
            email?: string;
            admin?: boolean;
        }
    }
}