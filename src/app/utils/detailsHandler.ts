export class DetailsHandler {
    private _prefix: string;
    private _details: HTMLCollectionOf<HTMLDetailsElement>;

    constructor(prefix: string) {
        this._prefix = prefix;
        this._details = document.getElementsByTagName("details");

        for (const detail of this._details) {
            this._loadStatus(detail);
            detail.addEventListener("toggle", e => this._storeStatus(e));
        }
    }

    private _loadStatus(detail: HTMLDetailsElement) {
        if (!detail.id) return;
        const item = sessionStorage.getItem(`${this._prefix}${detail.id}`);
        if (!item) return;
        detail.open = item === "open";
    }

    private _storeStatus(e: Event) {
        const target = e.target as HTMLDetailsElement | null;
        if (!target || !target.id) return;

        sessionStorage.setItem(`${this._prefix}${target.id}`, target.open ? "open" : "closed");
    }
}