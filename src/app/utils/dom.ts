type Size = {
    width: number,
    height: number,
};

const sizes = new Map<string, Promise<Size>>();

export function getElementById<T = HTMLElement>(id: string) {
    const ret = document.getElementById(id) as T | null;
    if (!ret) throw new Error(`${id} not found`);
    return ret;
}

export function getImageSize(url: string) {
    let promise = sizes.get(url);
    if (promise) return promise;

    promise = new Promise<Size>(resolve => {
        const image = document.createElement("img");
        image.addEventListener("load", () => {
            const size = { width: image.naturalWidth, height: image.naturalHeight };
            image.remove();
            resolve(size);
        });
        image.src = url;
    });
    sizes.set(url, promise);

    return promise;
}

export function getVideoSize(data: { video?: HTMLVideoElement, url?: string }) {
    let promise = data.url ? sizes.get(data.url) : undefined;
    if (promise) return promise;

    promise = new Promise<Size>((resolve, reject) => {
        if (data.video) {
            resolve({ width: data.video.videoWidth, height: data.video.videoHeight });
            return;
        } else if (data.url) {
            const video = document.createElement("video");
            video.addEventListener("loadedmetadata", () => {
                const size = { width: video.videoWidth, height: video.videoHeight };
                video.remove();
                resolve(size);
            });
            video.src = data.url;
        } else {
            reject();
        }
    });

    if (data.video) {
        sizes.set(data.video.src, promise);
    } else if (data.url) {
        sizes.set(data.url, promise);
    }

    return promise;
}