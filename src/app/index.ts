import { VTV } from "virtual-tour-viewer";
import "./index.scss";

new VTV({ baseUrl: "/", configUrl: "/api/config/tours/1", container: "viewer" });