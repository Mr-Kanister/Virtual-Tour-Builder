import { VTV } from "virtual-tour-viewer";
import { getElementById } from "../../utils/dom";
import "./navbar.scss"

const viewerDiv = getElementById<HTMLDivElement>("viewer");
const tourId = viewerDiv.getAttribute("tour-id")!;
const vtv = new VTV({ baseUrl: "/", configUrl: `/api/config/tours/${tourId}`, container: viewerDiv });
const viewer = await vtv.viewer;

const navbarForm = getElementById<HTMLFormElement>("navbar-form");

const items = navbarForm.getElementsByTagName("label");
let currentDraggedItem: HTMLLabelElement | null = null;
for (const item of items) {
    item.draggable = true;

    item.addEventListener("dragstart", (e: DragEvent) => {
        currentDraggedItem = item;

        for (const other of items) {
            if (other !== item) other.classList.add("hint");
        }
    });

    item.addEventListener("dragenter", (e: DragEvent) => {
        if (item !== currentDraggedItem) item.classList.add("active");
    });

    item.addEventListener("dragleave", (e: DragEvent) => {
        item.classList.remove("active");
    });

    item.addEventListener("dragend", (e: DragEvent) => {
        for (const other of items) {
            other.classList.remove("hint");
            other.classList.remove("active");
        }
    });

    item.addEventListener("dragover", (e: DragEvent) => e.preventDefault());

    item.addEventListener("drop", (e: DragEvent) => {
        e.preventDefault();
        if (!currentDraggedItem) return;

        if (item !== currentDraggedItem) {
            let oldPosition = 0;
            let newPosition = 0;

            // determine old and new positions
            for (let i = 0; i < items.length; i++) {
                if (currentDraggedItem === items[i]) oldPosition = i;
                if (item === items[i]) newPosition = i;
            }

            if (oldPosition < newPosition)
                item.parentNode!.insertBefore(currentDraggedItem, item.nextSibling);
            else
                item.parentElement!.insertBefore(currentDraggedItem, item);
        }
        updateConfig();
    });
}

navbarForm.addEventListener("input", e => updateConfig());

navbarForm.addEventListener("submit", async e => {
    e.preventDefault();

    const viewerConfig = {
        ...(await vtv.config).viewerConfig,
        plugins: undefined,
        container: undefined,
        adapter: undefined,
    };

    viewerConfig.navbar = viewer.config.navbar;

    const res = await fetch(`/api/tours/${tourId}`, {
        method: "PATCH",
        body: JSON.stringify({
            viewerConfig: JSON.stringify(viewerConfig),
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    });

    if (!res.ok) {
        alert(`Error: ${res.status}`);
        return;
    } else {
        window.location.assign(`/app/tours/${tourId}/settings`);
    }
});

function updateConfig() {
    const items = navbarForm.getElementsByTagName("label");
    const array: string[] = [];

    for (let i = 0; i < items.length; i++) {
        const input = items[i].firstElementChild as HTMLInputElement;
        if (input.checked) array.push(input.name);
    }

    viewer.setOption("navbar", array);
}