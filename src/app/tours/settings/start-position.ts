import { VTV } from "virtual-tour-viewer";
import { getElementById } from "../../utils/dom";
import "./start-position.scss";

const viewerDiv = getElementById<HTMLDivElement>("viewer");
const tourId = viewerDiv.getAttribute("tour-id")!;
const vtv = new VTV({ baseUrl: "/", configUrl: `/api/config/tours/${tourId}`, container: viewerDiv });
const viewer = await vtv.viewer;

viewer.addEventListener("click", e => {
    submit(e.data.yaw, e.data.pitch);
});

async function submit(yaw: number, pitch: number) {
    const viewerConfig = {
        ...(await vtv.config).viewerConfig,
        plugins: undefined,
        container: undefined,
        adapter: undefined,
    };
    viewerConfig.defaultPitch = pitch;
    viewerConfig.defaultYaw = yaw;

    const res = await fetch(`/api/tours/${tourId}`, {
        method: "PATCH",
        body: JSON.stringify({
            viewerConfig: JSON.stringify(viewerConfig),
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    });

    if (!res.ok) {
        alert(`Error: ${res.status}`);
        return;
    } else {
        window.location.assign(`/app/tours/${tourId}/settings`);
    }
}