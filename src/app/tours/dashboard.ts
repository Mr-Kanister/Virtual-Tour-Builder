import { VTV } from "virtual-tour-viewer";
import "./dashboard.scss";
export { };

const viewer = document.getElementById("viewer") as HTMLDivElement | null;
if (viewer) {
    const tourId = viewer.attributes.getNamedItem("tour-id")?.value!;
    new VTV({ baseUrl: "/", configUrl: `/api/config/tours/${tourId}?nocache=true`, container: viewer });
}