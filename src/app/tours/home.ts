export {};

const addTourButton = document.getElementById("add-tour") as HTMLButtonElement | null;
if (!addTourButton) throw new Error("add-tour not found");

async function send(e: Event) {
    e.preventDefault();

    const form = e.currentTarget as HTMLFormElement | null;
    if (!form) {
        throw new Error("current target is empty");
    }

    const data = new FormData(form);
    const submit = {
        name: data.get("name"),
        useCDN: data.get("useCDN"),
        type: data.get("type"),
    }

    const res = await fetch(form.action, {
        method: "POST",
        body: JSON.stringify(submit),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
          },
    });
    if (!res.ok) {
        alert(`Error: ${res.status}`);
    } else {
        window.location.reload();
    }
}

function add(e: Event) {
    // disable button
    const addTourButton = e.currentTarget as HTMLButtonElement | null;
    if (!addTourButton) throw new Error("current target is empty");
    addTourButton.disabled = true;

    // show edit form
    const form = document.getElementById("add-tour-form");
    if (!form) {
        throw new Error("add-tour-form not found");
    }
    form.classList.remove("hidden");

    // focus input field
    const firstChild = form.firstElementChild?.firstElementChild;
    if (firstChild instanceof HTMLInputElement) {
        firstChild.focus();
        firstChild.select();
    }

    // register event listener for form submission
    form.addEventListener("submit", send)
}

addTourButton.addEventListener("click", add, { once: true });