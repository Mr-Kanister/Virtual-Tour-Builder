import { getElementById } from "../utils/dom";
export { };

async function send(e: Event) {
    e.preventDefault();

    const form = e.currentTarget as HTMLFormElement | null;
    if (!form) {
        throw new Error("current target is empty");
    }

    const data = new FormData(form);
    const patch = {
        name: data.get("name") ?? undefined,
        useCDN: data.get("cdn") ?? undefined,
        introAnimation: data.get("intro-animation") ?? undefined,
    }

    const res = await fetch(form.action, {
        method: "PATCH",
        body: JSON.stringify(patch),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    });
    if (!res.ok) {
        alert(`Error: ${res.status}`);
    } else {
        window.location.reload();
    }
}

function edit(id: string) {
    return (e: Event) => {
        // disable button
        const editButton = e.currentTarget as HTMLButtonElement | null;
        if (!editButton) throw new Error("current target is empty");
        editButton.disabled = true;

        // hide current setting
        const current = document.getElementById(id);
        if (current) {
            current.hidden = true;
        };

        // show edit form
        const form = document.getElementById(`${id}-form`);
        if (!form) {
            throw new Error(`${id}-form not found`);
        }
        form.hidden = false;

        // focus input field
        const firstChild = form.firstElementChild;
        if (firstChild instanceof HTMLInputElement) {
            firstChild.focus();
            firstChild.select();
        }

        // register event listener for form submission
        form.addEventListener("submit", send)
    }
}

getElementById("name-edit-button").addEventListener("click", edit("name"), { once: true });
getElementById("cdn-edit-button").addEventListener("click", edit("cdn"), { once: true });
getElementById("intro-animation-edit-button").addEventListener("click", edit("intro-animation"), { once: true });

const tourDeleteForm = document.getElementById("tour-delete-form") as HTMLFormElement | null;
if (!tourDeleteForm) throw new Error("tour-delete-form not found");

tourDeleteForm.addEventListener("submit", async (e: Event) => {
    e.preventDefault();

    const form = e.currentTarget as HTMLFormElement | null;
    if (!form) {
        throw new Error("current target is empty");
    }

    const data = new FormData(form);
    if (data.get("tour-delete-checkbox") && confirm("Do you really want to permanently delete this tour including all its nodes and contents?")) {
        const res = await fetch(form.action, {
            method: "DELETE",
        });

        if (!res.ok) {
            alert(`Error: ${res.status}`);
        } else {
            window.location.assign("/app/tours/home");
        }
    }
});