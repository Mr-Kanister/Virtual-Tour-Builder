export { };

const deleteUserButtons = document.getElementsByClassName("delete-user") as HTMLCollectionOf<HTMLButtonElement>;

for (let i = 0; i < deleteUserButtons.length; i++) {
    deleteUserButtons.item(i)!.addEventListener("click", async (e: Event) => {
        e.stopPropagation();
        if (!confirm("Do you really want to delete this user?")) return;

        const deleteUserButton = e.currentTarget as HTMLButtonElement | null;
        if (!deleteUserButton) throw new Error("current target is empty");

        const userId = deleteUserButton.getAttribute("user-id");
        if (!userId) throw new Error("user-id attribute not found");

        const res = await fetch(`/api/users/${userId}`, {
            method: "DELETE",
        });

        if (!res.ok) {
            alert(`Error: ${res.status}`);
        } else {
            window.location.reload();
        }
    });
}

const addUserButton = document.getElementById("add-user") as HTMLButtonElement | null;
if (!addUserButton) throw new Error("add-user not found");

async function send(e: Event) {
    e.preventDefault();

    const form = e.currentTarget as HTMLFormElement | null;
    if (!form) {
        throw new Error("current target is empty");
    }

    const data = new FormData(form);
    const submit = {
        username: data.get("username"),
        displayname: data.get("displayname"),
        password: data.get("password"),
    }

    const res = await fetch(form.action, {
        method: "POST",
        body: JSON.stringify(submit),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    });
    if (!res.ok) {
        alert(`Error: ${res.status}`);
    } else {
        window.location.reload();
    }
}

function add(e: Event) {
    // disable button
    const addUserButton = e.currentTarget as HTMLButtonElement | null;
    if (!addUserButton) throw new Error("current target is empty");
    addUserButton.disabled = true;

    // show edit form
    const form = document.getElementById("add-user-form");
    if (!form) {
        throw new Error("add-user-form not found");
    }
    form.classList.remove("hidden");

    // register event listener for form submission
    form.addEventListener("submit", send)
}

addUserButton.addEventListener("click", add, { once: true });