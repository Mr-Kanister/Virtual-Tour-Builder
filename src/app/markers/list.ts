import { getElementById } from "../utils/dom";
export { };

function filter(filter: string) {
    const table = getElementById<HTMLDivElement>("markers-table");
    const children = table.children;

    for (let i = 1; i < children.length; i++) {
        const row = children[i] as HTMLAnchorElement;
        const tooltip = (row.children[1] as HTMLDivElement).textContent;
        const sidepanelContent = (row.children[2] as HTMLDivElement).textContent;
        const type = (row.children[3] as HTMLDivElement).textContent;
        const parent = (row.children[4] as HTMLDivElement).textContent;
        if ((tooltip?.toUpperCase().indexOf(filter.toUpperCase()) ?? -1) > -1 ||
            (sidepanelContent?.toUpperCase().indexOf(filter.toUpperCase()) ?? -1) > -1 ||
            (type?.toUpperCase().indexOf(filter.toUpperCase()) ?? -1) > -1 ||
            (parent?.toUpperCase().indexOf(filter.toUpperCase()) ?? -1) > -1) {
            row.style.display = "";
        } else {
            row.style.display = "none";
        }
    }
}


filter(getElementById<HTMLInputElement>("filter").value);
getElementById("filter").addEventListener("input", e => filter((e.target as HTMLInputElement).value));