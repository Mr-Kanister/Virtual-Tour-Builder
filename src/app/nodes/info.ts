import { VTV } from "virtual-tour-viewer";
import { getElementById } from "../utils/dom";
import { VirtualTourNode, VirtualTourPlugin } from "@photo-sphere-viewer/virtual-tour-plugin";
import * as submit from "./components/submit";
import "./info.scss";
import { DetailsHandler } from "../utils/detailsHandler";
export { };

const viewerDiv = getElementById<HTMLDivElement>("viewer");
const nodeId = viewerDiv.getAttribute("node-id")!;
const tourId = viewerDiv.getAttribute("tour-id")!;

let node: VirtualTourNode | undefined;

function editBasic(id: string, submitFn: (e: Event) => void) {
    return (e: Event) => {
        // disable button
        const editButton = e.currentTarget as HTMLButtonElement | null;
        if (!editButton) throw new Error("current target is empty");
        editButton.disabled = true;

        // hide current setting
        const current = getElementById(id);
        current.hidden = true;

        // show edit form
        const form = getElementById(`${id}-form`);
        form.hidden = false;

        // focus input field
        const firstChild = form.firstElementChild;
        if (firstChild instanceof HTMLInputElement) {
            firstChild.focus();
            firstChild.select();
        }

        // register event listener for form submission
        form.addEventListener("submit", e => {
            (e.submitter as HTMLInputElement).disabled = true;
            submitFn(e);
        });
    }
}

getElementById("name-edit-button")
    .addEventListener("click", editBasic("name", submit.basic()), { once: true });

getElementById("description-edit-button")
    .addEventListener("click", editBasic("description", submit.basic()), { once: true });

getElementById("caption-edit-button")
    .addEventListener("click", editBasic("caption", submit.basic()), { once: true });

getElementById("panorama-edit-button")
    .addEventListener("click", editBasic("panorama", e => submit.panorama(e)), { once: true });

getElementById("hotspot-edit-button")
    .addEventListener("click", editBasic("hotspot", submit.hotspot(Number(nodeId))), { once: true });

getElementById("node-delete-form").addEventListener("submit", async (e: Event) => {
    e.preventDefault();

    const form = e.currentTarget as HTMLFormElement | null;
    if (!form) {
        throw new Error("current target is empty");
    }

    const data = new FormData(form);
    if (data.get("node-delete-checkbox") && confirm("Do you really want to permanently delete this node including all its contents?")) {
        const res = await fetch(form.action, {
            method: "DELETE",
        });

        if (!res.ok) {
            alert(`Error: ${res.status}`);
        } else {
            window.location.assign("/app/nodes");
        }
    }
});

new DetailsHandler("nodes-info-");

new VTV({
    baseUrl: "/",
    configUrl: `/api/config/tours/${tourId}?nocache=true`,
    container: viewerDiv,
    startNodeId: nodeId
}).viewer
    .then(viewer => {
        node = viewer.getPlugin<VirtualTourPlugin>("virtual-tour").getCurrentNode();
    });