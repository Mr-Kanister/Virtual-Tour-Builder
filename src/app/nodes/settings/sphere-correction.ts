import { MarkersPlugin } from "@photo-sphere-viewer/markers-plugin";
import { getElementById } from "../../utils/dom";
import { VTV } from "virtual-tour-viewer";
import { Position } from "@photo-sphere-viewer/core";
import * as submit from "../components/submit";
import "./sphere-correction.scss";

const panLock = getElementById<HTMLInputElement>("sphere-correction-lock");
const panSlider = getElementById<HTMLInputElement>("sphere-correction-pan");
const tiltSlider = getElementById<HTMLInputElement>("sphere-correction-tilt");
const rollSlider = getElementById<HTMLInputElement>("sphere-correction-roll");

const viewerDiv = getElementById<HTMLDivElement>("viewer");
const nodeId = viewerDiv.getAttribute("node-id")!;
const viewer = await new VTV({ baseUrl: "/", configUrl: `/api/config/nodes/${nodeId}?nocache=true`, container: viewerDiv }).viewer
    .then(viewer => {
        panLock.disabled = false;
        tiltSlider.disabled = false;
        rollSlider.disabled = false;
        return viewer;
    });

const markersPlugin = (viewer.getPlugin("markers") as MarkersPlugin);

setCompass(markersPlugin);

let lockedPosition: Position | null = null;
let lockedPan: number;

panSlider.value = viewer.config.sphereCorrection?.pan?.toString() ?? "0";
tiltSlider.value = viewer.config.sphereCorrection?.tilt?.toString() ?? "0";
rollSlider.value = viewer.config.sphereCorrection?.roll?.toString() ?? "0";

panLock.addEventListener("input", e => {
    if ((e.target as HTMLInputElement).checked) {
        lockedPosition = viewer.getPosition();
        lockedPan = Number(viewer.config.sphereCorrection?.pan ?? 0);
    } else {
        lockedPosition = null;
    }
});

viewer.addEventListener("position-updated", e => {
    if (lockedPosition) {
        const pan = lockedPan + lockedPosition.yaw - e.position.yaw;
        const correction = {
            ...viewer.config.sphereCorrection,
            pan,
        };
        viewer.setOption("sphereCorrection", correction);
        const htmlValue = ((pan + 9.423) % 6.282) - 3.141;
        panSlider.value = htmlValue.toString();
    }
    markersPlugin.updateMarker({
        id: "horizon",
        polyline: [
            [e.position.yaw - 1.5, 0], [e.position.yaw + 1.5, 0],
        ],
    });
});

tiltSlider.addEventListener("input", e => {
    const value = (e.target as HTMLInputElement).value;
    Number(value);
    const correction = {
        ...viewer.config.sphereCorrection,
        tilt: Number(value),
    };
    viewer.setOption("sphereCorrection", correction);
});

rollSlider.addEventListener("input", e => {
    const value = (e.target as HTMLInputElement).value;
    Number(value);
    const correction = {
        ...viewer.config.sphereCorrection,
        roll: Number(value),
    };
    viewer.setOption("sphereCorrection", correction);
});

getElementById("sphere-correction-form").addEventListener("submit", submit.sphereCorrection(`/app/nodes/${nodeId}`));


function setCompass(markersPlugin: MarkersPlugin) {
    markersPlugin.addMarker({
        id: "horizon",
        polyline: [
            ["271deg", "0deg"], ["89deg", "0deg"],
        ],
        svgStyle: {
            stroke: 'rgba(255, 0, 0, 0.8)',
            strokeLinecap: 'round',
            strokeLinejoin: 'round',
            strokeWidth: '2px',
        },
    });

    markersPlugin.addMarker({
        id: "north",
        position: { pitch: 0, yaw: 0 },
        anchor: "top",
        html: `<strong>N</strong>`,
    });
    markersPlugin.addMarker({
        id: "north-east",
        position: { pitch: 0, yaw: "45deg" },
        anchor: "top",
        html: `<strong>NE</strong>`,
    });
    markersPlugin.addMarker({
        id: "east",
        position: { pitch: 0, yaw: "90deg" },
        anchor: "top",
        html: `<strong>E</strong>`,
    });
    markersPlugin.addMarker({
        id: "south-east",
        position: { pitch: 0, yaw: "135deg" },
        anchor: "top",
        html: `<strong>SE</strong>`,
    });
    markersPlugin.addMarker({
        id: "south",
        position: { pitch: 0, yaw: "180deg" },
        anchor: "top",
        html: `<strong>S</strong>`,
    });
    markersPlugin.addMarker({
        id: "south-west",
        position: { pitch: 0, yaw: "225deg" },
        anchor: "top",
        html: `<strong>SW</strong>`,
    });
    markersPlugin.addMarker({
        id: "west",
        position: { pitch: 0, yaw: "270deg" },
        anchor: "top",
        html: `<strong>W</strong>`,
    });
    markersPlugin.addMarker({
        id: "north-west",
        position: { pitch: 0, yaw: "315deg" },
        anchor: "top",
        html: `<strong>NW</strong>`,
    });
}