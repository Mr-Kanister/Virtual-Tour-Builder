import "./markers.scss";
import { Marker, MarkerConfig, MarkersPlugin } from "@photo-sphere-viewer/markers-plugin";
import { getElementById, getVideoSize, getImageSize } from "../../utils/dom";
import { ExtendedPosition, utils, events, Viewer } from "@photo-sphere-viewer/core";
import { VTV } from "virtual-tour-viewer";

const editor = {
    div: getElementById<HTMLDivElement>("markers-edit-div"),
    type: getElementById<HTMLTableCellElement>("marker-type"),
    file: getElementById<HTMLInputElement>("marker-file"),
    tooltip: getElementById<HTMLInputElement>("marker-tooltip"),
    position: {
        value: getElementById<HTMLInputElement>("marker-position"),
        setMiddle: getElementById<HTMLButtonElement>("marker-position-middle"),
        setCorners: getElementById<HTMLButtonElement>("marker-position-corners"),
    },
    size: {
        height: getElementById<HTMLInputElement>("marker-height"),
        width: getElementById<HTMLInputElement>("marker-width"),
    },
    sizeRange: getElementById<HTMLInputElement>("marker-size"),
    rotation: {
        yaw: getElementById<HTMLInputElement>("marker-yaw"),
        pitch: getElementById<HTMLInputElement>("marker-pitch"),
        roll: getElementById<HTMLInputElement>("marker-roll"),
    },
    hoverScale: getElementById<HTMLInputElement>("marker-hover-scale"),
    opacity: getElementById<HTMLInputElement>("marker-opacity"),
    svgStyle: {
        fill: {
            color: getElementById<HTMLInputElement>("marker-fill-color"),
            alpha: getElementById<HTMLInputElement>("marker-fill-alpha"),
        },
        stroke: {
            color: getElementById<HTMLInputElement>("marker-stroke-color"),
            alpha: getElementById<HTMLInputElement>("marker-stroke-alpha"),
        },
        strokeWidth: getElementById<HTMLInputElement>("marker-stroke-width"),
    },
    chromaKey: {
        active: getElementById<HTMLInputElement>("marker-chroma-active"),
        color: getElementById<HTMLInputElement>("marker-chroma-color"),
        similarity: getElementById<HTMLInputElement>("marker-chroma-similarity"),
        smoothness: getElementById<HTMLInputElement>("marker-chroma-smoothness"),
    },
    content: getElementById<HTMLTextAreaElement>("marker-content"),
    autoplay: getElementById<HTMLInputElement>("marker-autoplay"),
    // rect: {
    //     height: getElementById<HTMLInputElement>("marker-rect-height"),
    //     width: getElementById<HTMLInputElement>("marker-rect-width"),
    // },
    // ellipse: {
    //     rx: getElementById<HTMLInputElement>("marker-ellipse-x"),
    //     ry: getElementById<HTMLInputElement>("marker-ellipse-y"),
    // },
    html: getElementById<HTMLInputElement>("marker-html"),
    iframe: getElementById<HTMLInputElement>("marker-iframe"),
    clickUrl: getElementById<HTMLInputElement>("marker-click-url"),
    submit: getElementById<HTMLButtonElement>("marker-submit"),
    remove: getElementById<HTMLButtonElement>("marker-remove"),
};

let currentClickCallback: ((e: Event) => void) | null = null;
let currentInputCallback: ((e: Event) => void) | null = null;
let currentNewMarkerConfig: MarkerConfig | null = null;
let currentPolyline: { max?: number, values: [number, number][] } | null = null;

const viewerDiv = getElementById<HTMLDivElement>("viewer");
const nodeId = viewerDiv.getAttribute("node-id")!;

const viewer = await new VTV({ baseUrl: "/", configUrl: `/api/config/nodes/${nodeId}?nocache=true`, container: viewerDiv }).viewer
    .then(viewer => {
        getElementById<HTMLInputElement>("add-marker").disabled = false;
        return viewer;
    });
const markersPlugin = viewer.getPlugin<MarkersPlugin>("markers");

markersPlugin.addEventListener("select-marker", ({ marker }) => {
    if (marker.id === "position-marker") return;
    currentNewMarkerConfig = null;
    showOptions(marker.config, marker.type, viewer, markersPlugin);
});

viewer.addEventListener("click", draw(markersPlugin));

getElementById("marker-form").addEventListener("submit", e => {
    e.preventDefault();
    const form = e.currentTarget as HTMLFormElement | null;
    if (!form) {
        throw new Error("current target is empty");
    }
    const data = new FormData(form);
    let type = data.get("type") as string;

    let config: MarkerConfig = {
        id: Math.floor(Math.random() * 1000000).toString(),
        data: {
            new: true,
        },
    };

    if (type === "info" || type === "link") {
        config = {
            ...config,
            image: `/assets/icons/pin-yellow-${type}.png`,
            size: {
                height: 60,
                width: 60,
            },
        };
        type = "image";
    }

    showOptions(config, type, viewer, markersPlugin);
});

editor.submit.addEventListener("click", async e => {
    (e.target as HTMLButtonElement).disabled = true;
    submit(markersPlugin, Number(nodeId));
});

async function submit(markersPlugin: MarkersPlugin, node: number) {
    const markers = markersPlugin.getMarkers().map(m => m.config);
    const allFetches: Promise<void>[] = [];

    markers.forEach(marker => {
        if (marker.visible === false) {
            // delete marker
            allFetches.push(new Promise(async (resolve, reject) => {
                let res = await fetch(`/api/markers/${marker.id}`, {
                    method: "DELETE",
                });

                if (!res.ok) {
                    alert(`Error: ${res.status}`);
                    reject();
                } else {
                    resolve();
                }
                return;
            }));
        }
        // iframes get stored as html markers
        if (marker.elementLayer) {
            marker.html = "a";
            delete marker.elementLayer;
        }

        if (marker.id === "position-marker")
            return;

        const markerIsNew = marker.data?.new;

        const patch = {
            ...marker,
            // don't patch blob urls
            image: marker.image?.startsWith("/assets/icons/") ? marker.image : marker.image ? `/assets/markers/${marker.id}.png` : undefined,
            imageLayer: marker.imageLayer ? `/assets/markers/${marker.id}.png` : undefined,
            videoLayer: marker.videoLayer ? `/assets/markers/${marker.id}.mp4` : undefined,
            data: {
                iframe: marker.data?.iframe ?? undefined,
            },
        };

        if (patch.data?.iframe?.needsRefresh !== undefined) {
            patch.data.iframe.needsRefresh = true;
        }

        allFetches.push(new Promise(async (resolve, reject) => {
            let res = await fetch(`/api/markers/${markerIsNew ? "" : marker.id}`, {
                method: markerIsNew ? "POST" : "PATCH",
                body: JSON.stringify({
                    node_id: node,
                    config: JSON.stringify(patch),
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                },
            });

            if (!res.ok) {
                alert(`Error: ${res.status}`);
                reject();
                return;
            }

            // use created ID if marker was new
            if (markerIsNew) {
                marker.id = (await res.json()).id.toString();
                patch.id = marker.id;
                res = await fetch(`/api/markers/${marker.id}`, {
                    method: "PATCH",
                    body: JSON.stringify({
                        node_id: node,
                        config: JSON.stringify(patch),
                    }),
                    headers: {
                        "Content-type": "application/json; charset=UTF-8",
                    },
                });
            }

            // update paths to the files with new id if marker is new
            if (markerIsNew) {
                if (marker.image && !marker.image.startsWith("/assets/icons/")) {
                    patch.image = `/assets/markers/${marker.id}.png`;
                    res = await fetch(`/api/markers/${marker.id}`, {
                        method: "PATCH",
                        body: JSON.stringify({
                            node_id: node,
                            config: JSON.stringify(patch),
                        }),
                        headers: {
                            "Content-type": "application/json; charset=UTF-8",
                        },
                    });
                } else if (marker.imageLayer) {
                    patch.imageLayer = `/assets/markers/${marker.id}.png`;
                    res = await fetch(`/api/markers/${marker.id}`, {
                        method: "PATCH",
                        body: JSON.stringify({
                            node_id: node,
                            config: JSON.stringify(patch),
                        }),
                        headers: {
                            "Content-type": "application/json; charset=UTF-8",
                        },
                    });
                } else if (marker.videoLayer) {
                    patch.videoLayer = `/assets/markers/${marker.id}.mp4`;
                    res = await fetch(`/api/markers/${marker.id}`, {
                        method: "PATCH",
                        body: JSON.stringify({
                            node_id: node,
                            config: JSON.stringify(patch),
                        }),
                        headers: {
                            "Content-type": "application/json; charset=UTF-8",
                        },
                    });
                }

                if (!res.ok) {
                    alert(`Error: ${res.status}`);
                    reject();
                    return;
                }
            }

            // post new file if needed
            if (marker.data?.file) {
                const data = new FormData();
                data.append("file", marker.data.file);

                res = await fetch(`/api/upload/markers/${marker.id}`, {
                    method: "PUT",
                    body: data,
                });

                if (!res.ok) {
                    alert(`Error: ${res.status}`);
                    reject();
                    return;
                }
            }

            resolve();
        }));
    });

    await Promise.all(allFetches);
    window.location.assign(`/app/nodes/${nodeId}`);
}

function hideAllRows() {
    const table = editor.div.firstElementChild as HTMLTableElement;
    for (let i = 0; i < table.rows.length; i++) {
        table.rows[i]!.classList.add("hidden");
    }
}

function showRows(...elements: (HTMLElement | Object)[]) {
    elements.forEach(elem => {
        let row: Object | HTMLElement | null = elem;

        while (!(row instanceof HTMLElement)) {
            if (row === null) break;
            row = Object.values(row)[0];
        }

        row = row?.closest("tr") ?? null;

        if (row) (row as HTMLTableRowElement).classList.remove("hidden");
    });
}

function showValue(from: string | number | undefined, to: HTMLElement) {
    if (to instanceof HTMLInputElement || to instanceof HTMLTextAreaElement) {
        to.value = from?.toString() ?? "";
    } else {
        to.innerText = from?.toString() ?? "";
    }
}

function showOptions(config: MarkerConfig, type: string, viewer: Viewer, markersPlugin: MarkersPlugin) {
    if (currentClickCallback) {
        editor.div.removeEventListener("click", currentClickCallback);
    }
    if (currentInputCallback) {
        editor.div.removeEventListener("input", currentInputCallback);
    }
    currentNewMarkerConfig = config;
    currentClickCallback = positionMarker(config.id, type, viewer, markersPlugin);
    currentInputCallback = updateMarker(config.id, type, markersPlugin);
    editor.div.addEventListener("click", currentClickCallback);
    editor.div.addEventListener("input", currentInputCallback);

    hideAllRows();
    showRows(
        editor.type,
        editor.remove,
    );

    showValue(type, editor.type);
    showValue(config.opacity ?? 1, editor.opacity);

    if (type !== "elementLayer") {
        showRows(
            editor.tooltip,
            editor.content,
        );
        if (typeof config.tooltip === "string") {
            showValue(config.tooltip, editor.tooltip);
        } else {
            showValue(config.tooltip?.content, editor.tooltip);
        }
        showValue(config.content, editor.content);
    }

    if (type !== "polygon" && type !== "polyline") {
        showRows(editor.opacity);
    }

    switch (type) {
        case "image":
        case "imageLayer":
            editor.file.accept = "image/*";
            showRows(editor.file);
            break;
        case "videoLayer":
            showRows(editor.file);
            editor.file.accept = "video/*";
            break;
        case "elementLayer":
            showRows(editor.iframe);
            showValue(config.data?.iframe?.src, editor.iframe);
            break;
        case "polygon":
        case "polyline":
            break;
        // case "rect":
        //     showRows(editor.rect);
        //     showValue((config.rect as { width: number, height: number } | undefined)
        //         ?.height, editor.rect.height);
        //     showValue((config.rect as { width: number, height: number } | undefined)
        //         ?.width, editor.rect.width);
        //     break;
        // case "ellipse":
        //     showRows(editor.ellipse);
        //     showValue((config.ellipse as { rx: number, ry: number } | undefined)
        //         ?.rx, editor.ellipse.rx);
        //     showValue((config.rect as { rx: number, ry: number } | undefined)
        //         ?.ry, editor.ellipse.ry);
        //     break;
        case "html":
            showRows(editor.html);
            showValue(config.html, editor.html);
            break;
        default:
            console.warn("Marker not supported:");
            console.warn(config);
            return;
    }

    editor.file.value = "";

    switch (type) {
        // case "ellipse":
        // case "rect":
        //     showRows(
        //         editor.hoverScale,
        //         editor.rotation,
        //         editor.position,
        //     );
        //     editor.rotation.pitch.disabled = true;
        //     editor.rotation.yaw.disabled = true;
        //     editor.rotation.roll.disabled = false;
        //     editor.position.setCorners.disabled = true;
        //     editor.position.setMiddle.disabled = false;

        //     editor.hoverScale.checked = config.hoverScale !== false;
        //     showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
        //         ?.pitch, editor.rotation.pitch);
        //     showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
        //         ?.yaw, editor.rotation.yaw);
        //     showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
        //         ?.roll, editor.rotation.roll);
        //     break;
        case "html":
        case "image":
            if (type === "html") {
                showRows(editor.size);
                showValue(config.size?.height, editor.size.height);
                showValue(config.size?.width, editor.size.width);
            }
            else {
                showRows(editor.sizeRange);
                showValue(config.size?.width, editor.sizeRange);
            }

            showRows(
                editor.position,
                editor.rotation,
                editor.hoverScale,
                editor.clickUrl,
            );
            editor.rotation.pitch.disabled = true;
            editor.rotation.yaw.disabled = true;
            editor.rotation.roll.disabled = false;
            editor.position.setCorners.disabled = true;
            editor.position.setMiddle.disabled = false;

            editor.hoverScale.checked = config.hoverScale !== false;
            showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                ?.pitch, editor.rotation.pitch);
            showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                ?.yaw, editor.rotation.yaw);
            showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                ?.roll, editor.rotation.roll);
            showValue(config.data?.clickUrl, editor.clickUrl);
            break;
        case "videoLayer":
            showRows(editor.autoplay);
            editor.autoplay.checked = !(config.autoplay === false);
        case "imageLayer":
            showRows(
                editor.clickUrl,
                editor.position,
                editor.chromaKey,
            );

            editor.rotation.pitch.disabled = false;
            editor.rotation.yaw.disabled = false;
            editor.rotation.roll.disabled = false;

            // decide if marker was set using it's corners or it's center
            if (config.position instanceof Array) {
                // corners
                editor.position.setCorners.disabled = false;
                editor.position.setMiddle.disabled = true;
            } else if (config.position instanceof Object) {
                // center
                showRows(
                    editor.rotation,
                    editor.sizeRange,
                );
                editor.position.setCorners.disabled = true;
                editor.position.setMiddle.disabled = false;
                showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                    ?.pitch, editor.rotation.pitch);
                showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                    ?.yaw, editor.rotation.yaw);
                showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                    ?.roll, editor.rotation.roll);
                showValue(config.size?.width, editor.sizeRange);
            } else {
                // not yet defined
                showRows(
                    editor.rotation,
                    editor.sizeRange,
                );
                editor.position.setCorners.disabled = false;
                editor.position.setMiddle.disabled = false;
                showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                    ?.pitch, editor.rotation.pitch);
                showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                    ?.yaw, editor.rotation.yaw);
                showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                    ?.roll, editor.rotation.roll);
                showValue(config.size?.width, editor.sizeRange);
            }

            showValue(config.data?.clickUrl, editor.clickUrl);
            editor.chromaKey.active.checked = config.chromaKey?.enabled === true;
            showValue(config.chromaKey?.color, editor.chromaKey.color);
            showValue(config.chromaKey?.similarity, editor.chromaKey.similarity);
            showValue(config.chromaKey?.smoothness, editor.chromaKey.smoothness);
            break;
        case "elementLayer":
            showRows(
                editor.position,
                editor.rotation,
                editor.size,
            );
            editor.rotation.pitch.disabled = false;
            editor.rotation.yaw.disabled = false;
            editor.rotation.roll.disabled = false;
            editor.position.setCorners.disabled = true;
            editor.position.setMiddle.disabled = false;

            showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                ?.pitch, editor.rotation.pitch);
            showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                ?.yaw, editor.rotation.yaw);
            showValue((config.rotation as { roll: number, pitch: number, yaw: number } | undefined)
                ?.roll, editor.rotation.roll);
            showValue(config.size?.height, editor.size.height);
            showValue(config.size?.width, editor.size.width);
            break;
        case "polygon":
        case "polyline":
            showRows(
                editor.svgStyle,
                editor.position,
                editor.clickUrl,
                editor.hoverScale,
            );
            editor.position.setMiddle.disabled = true;
            editor.position.setCorners.disabled = false;
            showValue(config.data?.clickUrl, editor.clickUrl);
            editor.hoverScale.checked = config.hoverScale === true;

            const fillColor = splitRGBA(config.svgStyle?.fill);
            showValue(fillColor.rgb, editor.svgStyle.fill.color);
            showValue(fillColor.a, editor.svgStyle.fill.alpha);

            const strokeColor = splitRGBA(config.svgStyle?.stroke);
            showValue(strokeColor.rgb, editor.svgStyle.stroke.color);
            showValue(strokeColor.a, editor.svgStyle.stroke.alpha);

            showValue(parseInt(config.svgStyle?.strokeWidth ?? "1"), editor.svgStyle.strokeWidth);
            break;
    }

}

function splitRGBA(rgba?: string) {
    if (rgba) {
        const rgb = rgba.substring(0, 7);
        const a = Number(`0x${rgba.substring(7, 9)}`);
        return { rgb, a };
    }

    return { rgb: "#000000", a: 255 };
}

function mergeRGBA(rgb: string, a: number) {
    return rgb + (a === 255 ? "" : a.toString(16).padStart(2, "0"));
}

function updateMarker(id: string, type: string, markersPlugin: MarkersPlugin) {
    return async (e: Event) => {
        const target = e.target as HTMLInputElement;
        let patch: MarkerConfig = { id, visible: true };

        if (target.files && target.files.length) {
            const file = target.files[0];
            let blobURL = URL.createObjectURL(file);

            patch.data = {
                file,
            };

            switch (type) {
                case "image":
                    patch.image = blobURL;
                    break;
                case "imageLayer":
                    patch.imageLayer = blobURL;
                    break;
                case "videoLayer":
                    patch.videoLayer = blobURL;
                    break;
            }
        }

        if (target.id === "marker-position") {
            switch (type) {
                case "html":
                case "image":
                case "imageLayer":
                case "videoLayer":
                case "elementLayer":
                    const position = JSON.parse(target.value) as
                        ExtendedPosition | [number, number][];
                    if (position instanceof Array) {
                        patch.position = [{
                            yaw: position[0][0],
                            pitch: position[0][1],
                        },
                        {
                            yaw: position[1][0],
                            pitch: position[1][1],
                        },
                        {
                            yaw: position[2][0],
                            pitch: position[2][1],
                        },
                        {
                            yaw: position[3][0],
                            pitch: position[3][1],
                        }];
                    } else {
                        patch.position = position;
                    }
                    break;
                case "polygon":
                    patch.polygon = JSON.parse(target.value);
                    break;
                case "polyline":
                    patch.polyline = JSON.parse(target.value);
                    break;
            }
        }

        if (target.id === "marker-size") {
            let marker: Marker | null = null;
            let config: MarkerConfig | null = null;
            try {
                marker = markersPlugin.getMarker(id);
                config = marker.config;
            } catch (_) {
                config = currentNewMarkerConfig;
            }

            let ratio = 16 / 9;
            const newWidth = Number(editor.sizeRange.value);

            if (config) {
                if (type === "image" && config.image) {
                    const imageSize = await getImageSize(config.image);
                    ratio = imageSize.width / imageSize.height;
                } else if (type === "imageLayer" && config.imageLayer) {
                    const imageSize = await getImageSize(config.imageLayer);
                    ratio = imageSize.width / imageSize.height;
                } else if (type === "videoLayer" && config.videoLayer) {
                    const videoSize = await getVideoSize({
                        video: marker ? marker.video : undefined,
                        url: config.videoLayer
                    });
                    ratio = videoSize.width / videoSize.height;
                }
            }

            patch.size = {
                width: newWidth,
                height: newWidth / ratio,
            };
        }

        switch (target.id) {
            case "marker-tooltip":
                patch.tooltip = target.value;
                break;
            case "marker-height":
            case "marker-width":
                patch.size = {
                    width: Number(editor.size.width.value),
                    height: Number(editor.size.height.value),
                };

                if (type === "elementLayer") {
                    patch.data = {
                        iframe: {
                            needsRefresh: true,
                        },
                    };
                }
                break;
            case "marker-yaw":
            case "marker-pitch":
            case "marker-roll":
                patch.rotation = {
                    yaw: Number(editor.rotation.yaw.value),
                    pitch: Number(editor.rotation.pitch.value),
                    roll: Number(editor.rotation.roll.value),
                };
                break;
            case "marker-hover-scale":
                patch.hoverScale = target.checked ? true : false;
                break;
            case "marker-opacity":
                patch.opacity = Number(target.value);
                break;
            case "marker-fill-color":
            case "marker-fill-alpha":
            case "marker-stroke-color":
            case "marker-stroke-alpha":
            case "marker-stroke-width":
                patch.svgStyle = {
                    fill: mergeRGBA(editor.svgStyle.fill.color.value, Number(editor.svgStyle.fill.alpha.value)),
                    stroke: mergeRGBA(editor.svgStyle.stroke.color.value, Number(editor.svgStyle.stroke.alpha.value)),
                    strokeWidth: editor.svgStyle.strokeWidth.value + "px",
                };
                break;
            case "marker-chroma-active":
            case "marker-chroma-color":
            case "marker-chroma-similarity":
            case "marker-chroma-smoothness":
                patch.chromaKey = {
                    enabled: editor.chromaKey.active.checked,
                    color: editor.chromaKey.color.value,
                    similarity: Number(editor.chromaKey.similarity.value),
                    smoothness: Number(editor.chromaKey.smoothness.value),
                };
                break;
            case "marker-content":
                patch.content = target.value;
                break;
            case "marker-autoplay":
                patch.autoplay = target.checked;
                break;
            // case "marker-rect-height":
            // case "marker-rect-width":
            //     patch.rect = {
            //         height: Number(editor.rect.height.value),
            //         width: Number(editor.rect.width.value),
            //     };
            //     break;
            // case "marker-ellipse-x":
            // case "marker-ellipse-y":
            //     patch.ellipse = {
            //         rx: Number(editor.ellipse.rx),
            //         ry: Number(editor.ellipse.ry),
            //     };
            //     break;
            case "marker-html":
                patch.html = target.value;
                break;
            case "marker-iframe":
                patch.data = {
                    iframe: {
                        src: target.value,
                        needsRefresh: true,
                    },
                };

                break;
            case "marker-click-url":
                patch.data = {
                    clickUrl: target.value,
                };
                break;
        }

        try {
            markersPlugin.updateMarker(patch);
        } catch (e) {
            try {
                // element layer can't be added directly but only through html marker
                if (type === "elementLayer") {
                    patch.html = "a";
                    patch.visible = false;
                }
                utils.deepmerge(currentNewMarkerConfig, patch);
                markersPlugin.addMarker(currentNewMarkerConfig!);
            } catch (e) { }
        }
    }
}

function positionMarker(id: string, type: string, viewer: Viewer, markersPlugin: MarkersPlugin) {
    return (e: Event) => {
        const target = e.target as HTMLButtonElement;
        if (target.id === "marker-position-middle") {
            // disable both position buttons
            // this needs to be done because each marker can
            // only ever be defined using one method. The one
            // which is currently being used will get reenabled
            // after finishing the positioning.
            editor.position.setCorners.disabled = true;
            target.disabled = true;

            viewer.container.style.cursor = "crosshair";
            viewer.addEventListener("click", e => {
                editor.position.value.value = JSON.stringify({
                    yaw: e.data.yaw,
                    pitch: e.data.pitch,
                });
                editor.position.value.dispatchEvent(new Event("input", { bubbles: true }));
                target.disabled = false;
                viewer.container.style.cursor = "move";
                target.classList.add("completed");
                setTimeout(() => target.classList.remove("completed"), 2000);
            }, { once: true });
        } else if (target.id === "marker-position-corners") {
            if (currentPolyline) {
                finishDrawing(markersPlugin);
                return;
            }

            viewer.container.style.cursor = "crosshair";

            if (["imageLayer", "videoLayer"].includes(type)) {
                currentPolyline = { values: [], max: 4 };
                // disable both position buttons
                // this needs to be done because each marker can
                // only ever be defined using one method. The one
                // which is currently being used will get reenabled
                // after finishing the positioning.
                target.disabled = true;
                editor.position.setMiddle.disabled = true;

                // if the user decided on the corner positioning,
                // the size and rotation inputs can be hidden
                editor.size.height.closest("tr")?.classList.add("hidden");
                editor.rotation.pitch.closest("tr")?.classList.add("hidden");

            } else {
                // try to restore current polyline
                currentPolyline = { values: [] };
                try {
                    const marker = markersPlugin.getMarker(id);

                    switch (type) {
                        case "polygon":
                            currentPolyline.values = marker.config.polygon as [number, number][] ?? [];
                            break;
                        case "polyline":
                            currentPolyline.values = marker.config.polyline as [number, number][] ?? [];
                            break;
                    }

                    markersPlugin.addMarker({
                        id: "position-marker",
                        polyline: currentPolyline.values,
                        svgStyle: {
                            stroke: "#c80000cc",
                            strokeWidth: "3px",
                        },
                    });
                } catch (e) { }

                target.innerText = "Stop drawing";
            }

            // hide marker for redrawing
            try {
                markersPlugin.updateMarker({ id, visible: false });
            } catch (e) { }
        } else if (target.id === "marker-remove") {
            try {
                markersPlugin.updateMarker({
                    id,
                    visible: false,
                });
            } catch (e) { }

            hideAllRows();
        }
    };
};

function finishDrawing(markersPlugin: MarkersPlugin) {
    if (!currentPolyline) return;
    if (currentPolyline.values.length < 2) {
        console.warn("Not enough points set. Discarding.");
    } else {

        // set value of hidden input
        editor.position.value.value = JSON.stringify(currentPolyline.values);

        currentPolyline = null;
        markersPlugin.removeMarker("position-marker");

        // dispatch event which updates the target marker
        editor.position.value.dispatchEvent(new Event("input", { bubbles: true }));

        editor.position.setCorners.classList.add("completed");
        setTimeout(() => editor.position.setCorners.classList.remove("completed"), 2000);
    }
    // reset button
    editor.position.setCorners.disabled = false;
    editor.position.setCorners.innerText = "Set corner points";
    viewer.container.style.cursor = "move";
}

function draw(markersPlugin: MarkersPlugin) {
    return (e: events.ClickEvent) => {
        if (!currentPolyline) return;

        if (e.data.rightclick) {
            // remove last point
            currentPolyline.values.pop();
            if (currentPolyline.values.length < 2) {
                try {
                    markersPlugin.removeMarker("position-marker");
                } catch (e) { }
            } else {
                markersPlugin.updateMarker({
                    id: "position-marker",
                    polyline: currentPolyline.values,
                });
            }
        } else {
            // add point
            currentPolyline.values.push([e.data.yaw, e.data.pitch]);
            if (currentPolyline.values.length >= 2) {
                try {
                    markersPlugin.updateMarker({
                        id: "position-marker",
                        polyline: currentPolyline.values,
                    });
                } catch (e) {
                    markersPlugin.addMarker({
                        id: "position-marker",
                        polyline: currentPolyline.values,
                        svgStyle: {
                            stroke: "#c80000cc",
                            strokeWidth: "3px",
                        },
                    });
                }
            }
        }

        if (currentPolyline.max && currentPolyline.values.length >= currentPolyline.max) {
            finishDrawing(markersPlugin);
        }

    }
}