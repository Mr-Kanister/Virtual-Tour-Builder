import "./links.scss";
import { VTV } from "virtual-tour-viewer";
import { getElementById } from "../../utils/dom";
import * as linksMap from "../components/linksMap";
import { VirtualTourLink, VirtualTourPlugin } from "@photo-sphere-viewer/virtual-tour-plugin";

const viewerDiv = getElementById<HTMLDivElement>("viewer");
const nodeId = viewerDiv.getAttribute("node-id")!;
const tourId = viewerDiv.getAttribute("tour-id")!;

const viewer = await new VTV({
    baseUrl: "/",
    configUrl: `/api/config/tours/${tourId}?nocache=true`,
    container: viewerDiv,
    startNodeId: nodeId
}).viewer;
const virtualTourPlugin = viewer.getPlugin("virtual-tour") as VirtualTourPlugin;
const currentNode = virtualTourPlugin.getCurrentNode();
const links = currentNode.links ?? [];
const gps = (currentNode.gps ?? [11, 49]) as [number, number];

const map = linksMap.initMap(getElementById("map"), gps);
const table = getElementById<HTMLTableElement>("links-table");

for (let i = 1; i < table.rows.length; i++) {
    const row = table.rows.item(i)!;
    linksMap.addLink(map, row,);
    row.addEventListener("click", updateLink(nodeId, map, links, virtualTourPlugin));
    row.addEventListener("input", updateLink(nodeId, map, links, virtualTourPlugin));
}

getElementById("links-add").addEventListener("click", showAddableLinks(map, nodeId, tourId));
getElementById("links-submit").addEventListener("click", async () => {
    let failed = false;
    for (let i = 1; i < table.rows.length; i++) {
        const row = table.rows.item(i)!;

        if (row.classList.contains("removed")) {
            const id = row.attributes.getNamedItem("id")!.value;
            // removed
            const res = await fetch(`/api/links/${id}`, {
                method: "DELETE"
            });
            if (!res.ok) {
                failed = true;
                alert(`Error: ${res.status}`);
                break;
            }
        } else if (row.attributes.getNamedItem("id") && row.classList.contains("updated")) {
            // updated
            const id = row.attributes.getNamedItem("id")!.value;
            const tooltip = (row.cells[1].firstElementChild as HTMLInputElement).value || undefined;
            const offset = (row.cells[2].firstElementChild as HTMLInputElement).value;
            const depth = (row.cells[3].firstElementChild as HTMLInputElement).value;
            const res = await fetch(`/api/links/${id}`, {
                method: "PATCH",
                body: JSON.stringify({
                    linkOffset: `{"yaw": ${Number(offset)}, "depth": ${Number(depth)}}`,
                    data: tooltip ? JSON.stringify({ tooltip }) : undefined,
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                },
            });
            if (!res.ok) {
                failed = true;
                alert(`Error: ${res.status}`);
                break;
            }

        } else if (row.attributes.getNamedItem("id")) {
            // nothing to be done
            continue;
        } else {
            // created
            const targetId = row.attributes.getNamedItem("target-id")!.value;
            const tooltip = (row.cells[1].firstElementChild as HTMLInputElement).value || undefined;
            const offset = (row.cells[2].firstElementChild as HTMLInputElement).value || "0";
            const depth = (row.cells[3].firstElementChild as HTMLInputElement).value || "1";
            const res = await fetch(`/api/links`, {
                method: "POST",
                body: JSON.stringify({
                    node_from: Number(nodeId),
                    node_to: Number(targetId),
                    linkOffset: `{"yaw": ${Number(offset)}, "depth": ${Number(depth)}}`,
                    data: tooltip ? JSON.stringify({ tooltip }) : undefined,
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                },
            });
            if (!res.ok) {
                failed = true;
                alert(`Error: ${res.status}`);
                break;
            }
        }
    }
    if (!failed) window.location.assign(`/app/nodes/${nodeId}`);
});

function decodeHTMLEntities(text: string) {
    const textArea = document.createElement("textarea");
    textArea.innerHTML = text;
    return textArea.value;
}

function addLink(currentNodeId: string, map: L.Map, table: HTMLTableElement,
    virtualTourPlugin: VirtualTourPlugin, links: VirtualTourLink[], node: any) {
    return () => {
        linksMap.hideAddableLinks(map);

        const row = table.insertRow(-1);
        row.classList.add("updated");

        if (node.data?.loadTour) {
            row.setAttribute("target-id", node.data.loadTour.startNodeId);
        } else {
            row.setAttribute("target-id", node.id);
        }
        row.setAttribute("target-gps", node.gps);

        row.insertCell().innerHTML = node.name;

        const nameInput = document.createElement("input");
        nameInput.type = "text";
        if (node.data?.loadTour) {
            nameInput.value = decodeHTMLEntities(node.name);
        } else {
            nameInput.placeholder = decodeHTMLEntities(node.name);
        }
        row.insertCell().append(nameInput);

        const offsetInput = document.createElement("input");
        offsetInput.type = "range";
        offsetInput.min = "-3.14";
        offsetInput.max = "3.14";
        offsetInput.step = "0.01";
        offsetInput.value = "0";
        offsetInput.classList.add("max-parent");
        row.insertCell().append(offsetInput);

        const depthInput = document.createElement("input");
        depthInput.type = "range";
        depthInput.min = "0";
        depthInput.max = "3";
        depthInput.step = "0.01";
        depthInput.value = "1";
        depthInput.classList.add("max-parent");
        row.insertCell().append(depthInput);

        const removeButton = document.createElement("button");
        removeButton.innerText = "Remove";
        removeButton.classList.add("red-button");
        const removeButtonRow = row.insertCell();
        removeButtonRow.classList.add("centering");
        removeButtonRow.append(removeButton);

        linksMap.addLink(map, row);
        linksMap.showRemovedLinks(map);

        if (node.data?.loadTour) {
            links?.push({
                nodeId: node.id.toString(),
                data: {
                    loadTour: node.data.loadTour,
                    tooltip: node.name,
                },
                gps: JSON.parse(node.gps ?? "[0,0]"),
            });
        } else {
            links?.push({
                nodeId: node.id.toString(),
            });
        }
        virtualTourPlugin.updateNode({
            id: currentNodeId,
            links,
        });

        row.addEventListener("click", updateLink(currentNodeId, map, links, virtualTourPlugin));
        row.addEventListener("input", updateLink(currentNodeId, map, links, virtualTourPlugin));
    }
}

function showAddableLinks(map: L.Map, nodeId: string, currentTourId: string) {
    return async (e: Event) => {
        e.preventDefault();
        linksMap.hideAddableLinks(map);

        const selectedTourId = getElementById<HTMLSelectElement>("links-tour").value;
        const res = await fetch(`/api/nodes?tour=${selectedTourId}`);
        if (!res.ok) {
            alert(`Error ${res.status}`);
            return;
        }

        const nodes = await res.json();

        linksMap.hideRemovedLinks(map);
        nodes.forEach((node: any) => {
            if (node.id === Number(nodeId)) return;
            if (selectedTourId !== currentTourId) {
                node.data = {
                    ...node.data,
                    loadTour: {
                        configUrl: `/api/config/tours/${selectedTourId}`,
                        startNodeId: node.id.toString(),
                    },
                };
                node.id = "-1";
            }
            linksMap.showAddableLink(map, node, addLink, nodeId, table, virtualTourPlugin, links);
        });
    }
}

function updateLink(nodeId: string, map: L.Map,
    links: VirtualTourLink[], virtualTourPlugin: VirtualTourPlugin) {
    return (e: Event) => {
        e.preventDefault();
        const target = e.target as HTMLElement;
        const row = target.closest("tr");
        if (!row) return;

        const targetId = row.attributes.getNamedItem("target-id")!.value;
        const linkIndex = links.findIndex(link => {
            if (link.data?.loadTour) {
                return link.data.loadTour.startNodeId === targetId;
            }
            return link.nodeId === targetId;
        });

        if (target instanceof HTMLButtonElement) {
            // remove link
            linksMap.removeLink(map, targetId);
            if (row.attributes.getNamedItem("id")) {
                row.classList.add("removed");
                row.classList.remove("updated");
                target.disabled = true;
            } else {
                row.remove();
            }

            links?.splice(linkIndex, 1);
        } else if (e.type === "input") {
            // update link
            row.classList.add("updated");
            const tooltip = (row.cells[1].firstElementChild as HTMLInputElement).value || undefined;
            const offset = (row.cells[2].firstElementChild as HTMLInputElement).value
            const depth = (row.cells[3].firstElementChild as HTMLInputElement).value
            const link = links[linkIndex];
            link.linkOffset = { yaw: Number(offset), depth: Number(depth) };
            link.data = { ...(link.data ?? {}), tooltip };
            links[linkIndex] = link;
        } else {
            return;
        }

        virtualTourPlugin.updateNode({
            id: nodeId,
            links,
        });
    }
}