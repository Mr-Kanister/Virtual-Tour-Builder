import "./chains.scss";
import { getElementById } from "../../utils/dom";
import * as chainsMap from "../components/chainsMap";

let newRow: HTMLTableRowElement | null = null;
let markersAdded = false;
let fromGathered = false;

const mapDiv = getElementById("map");
const gps = JSON.parse(mapDiv.getAttribute("gps")!) as [number, number];
const nodeId = mapDiv.getAttribute("node-id")!;

const map = chainsMap.initMap(mapDiv, gps);
const table = getElementById<HTMLTableElement>("chains-table");

getElementById("add-chain").addEventListener("click", showAddableLinks(map, table, nodeId));
getElementById("chains-table").addEventListener("click", removeChain);
getElementById("chains-submit").addEventListener("click", submitChains(nodeId));


function gatherFromTo(map: L.Map, link: any) {
    if (!newRow) return;
    if (!fromGathered) {
        const cell = newRow.firstElementChild as HTMLTableCellElement;
        cell.innerText = link.name;
        newRow.setAttribute("from", link.node_to);
        fromGathered = true;

        // hide just clicked marker
        chainsMap.hideLink(map, link.node_to);
    } else {
        const cell = newRow.children[1] as HTMLTableCellElement
        cell.innerText = link.name;
        newRow.setAttribute("to", link.node_to);

        newRow = null;

        // hide markers
        chainsMap.hideLinks(map);

        // reenable new chain button
        getElementById<HTMLButtonElement>("add-chain").disabled = false;
    }
}

function removeChain(e: Event) {
    if (e.target instanceof HTMLButtonElement) {
        newRow = null;
        fromGathered = false;
        getElementById<HTMLButtonElement>("add-chain").disabled = false;
        e.target.closest("tr")?.remove();
    }
}

function submitChains(nodeId: string) {
    return async (e: Event) => {
        (e.target as HTMLButtonElement).disabled = true;

        let res = await fetch(`/api/nodes/${nodeId}`);

        if (!res.ok) {
            alert(`Error: ${res.status}`);
            return;
        }

        let data = JSON.parse((await res.json()).data);
        const target: Record<number, { to: number }> = {};

        const table = getElementById<HTMLTableElement>("chains-table");
        for (let i = 1; i < table.rows.length; i++) {
            const row = table.rows[i];
            const from = row.getAttribute("from");
            const to = row.getAttribute("to");

            if (!from || !to) continue;

            target[Number(from)] = {
                to: Number(to),
            };
        }

        if (data) {
            data.chains = target;
        } else {
            data = {
                chains: target,
            };
        }

        res = await fetch(`/api/nodes/${nodeId}`, {
            method: "PATCH",
            body: JSON.stringify({
                data: JSON.stringify(data)
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
        });

        if (!res.ok) {
            alert(`Error: ${res.status}`);
            return;
        } else {
            window.location.assign(`/app/nodes/${nodeId}`);
        }
    }
}

function showAddableLinks(map: L.Map, table: HTMLTableElement, nodeId: string) {
    return async (e: Event) => {
        e.preventDefault();
        chainsMap.hideLinks(map);

        if (!markersAdded) {
            const res = await fetch(`/api/links?node=${nodeId}`);
            if (!res.ok) {
                alert(`Error ${res.status}`);
                return;
            }
            const links = await res.json();

            chainsMap.addLinks(links, gatherFromTo);
            markersAdded = true;
        }

        // disable new chain button
        (e.target as HTMLButtonElement).disabled = true;

        // create new row
        newRow = table.insertRow(1);
        newRow.insertCell();
        newRow.insertCell();
        const removeButton = document.createElement("button");
        removeButton.innerText = "Remove";
        removeButton.classList.add("red-button");
        newRow.insertCell().append(removeButton);
        fromGathered = false;

        chainsMap.showLinks(map);
    }
}