import "./location.scss";
import L from "leaflet";
import { getElementById } from "../../utils/dom";
import * as submit from "../components/submit";

const map = getElementById("map");
const nodeId = map.getAttribute("node-id")!;
const gpsInput = getElementById<HTMLInputElement>("gps-input");
initMap(map, gpsInput, JSON.parse(gpsInput.value));

const mapForm = getElementById("gps-form");
mapForm.addEventListener("submit", submit.basic(`/app/nodes/${nodeId}`));
    

function initMap(container: string | HTMLElement, gpsInput: HTMLInputElement, gps?: [number, number]) {
    if (!gps) {
        gps = [49, 11];
        gpsInput.value = "[11, 49]";
    }

    // leaflet uses [lat, lng]
    // psv uses [lng, lat]
    gps = [gps[1], gps[0]];

    const map = L.map(container).setView(gps, 13);
    const marker = L.marker(gps)
    .addTo(map);

    L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 19,
        attribution: `&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>`
    })
    .addTo(map);

    map.on("click", e => {
        const latlng = e.latlng;
        marker.setLatLng(latlng);

        gpsInput.value = `[${latlng.lng}, ${latlng.lat}]`;
    });
}

// type MapConfig = {
//     tourId: number,
//     size: {
//         width: number,
//         height: number,
//     },
//     currentGps: [number, number],
//     extend: [number, number, number, number],
// };

// let mapConfig: MapConfig;

// export async function loadMapCanvas(canvas: HTMLCanvasElement) {
//     await loadMapConfig(canvas);

//     // set canvas dimensions
//     canvas.height = mapConfig.size.height;
//     canvas.width = mapConfig.size.width;

//     drawGpsDot(canvas, mapConfig);

//     canvas.addEventListener("click", updateGpsDot);
// }

// async function loadMapConfig(canvas: HTMLCanvasElement) {
//     if (mapConfig) return;

//     const tourId = Number(canvas.attributes.getNamedItem("node-id")?.value);
//     if (Number.isNaN(tourId)) throw new Error("node-id attribute not found");

//     const url = `/assets/maps/${tourId}.jpg`;
//     const size = await getMapSize(url);
//     const currentGps: [number, number] = JSON.parse(canvas.attributes.getNamedItem("position")?.value ?? "[0, 0]");
//     const extend: [number, number, number, number] = JSON.parse(canvas.attributes.getNamedItem("extend")?.value!);

//     mapConfig = {
//         tourId,
//         size,
//         currentGps,
//         extend,
//     };

//     return;
// }

// function drawGpsDot(
//     canvas: HTMLCanvasElement,
//     mapConfig: MapConfig,
// ) {
//     const {extend, currentGps, size} = mapConfig;
//     const context = canvas.getContext("2d");
//     if (!context) throw new Error("context not found");

//     const x = Math.floor(((currentGps[0] - extend[0]) /
//         (extend[2] - extend[0])) * size.width);
//     const y = Math.floor(((extend[1] - currentGps[1]) /
//         (extend[1] - extend[3])) * size.height);

//     context.clearRect(0, 0, context.canvas.width, context.canvas.height);
//     context.beginPath();
//     context.arc(x, y, 15, 0, 2 * Math.PI);
//     context.fillStyle = "rgba(0, 163, 255, 0.99)";
// }

// async function updateGpsDot(e: MouseEvent) {
//     const canvas = e.target as HTMLCanvasElement;
//     await loadMapCanvas(canvas);
//     const {extend, size} = mapConfig;

//     // Calculate x and y coordinates of click on map
//     const imgRect = canvas.getBoundingClientRect();
//     const clickX = Math.floor(e.clientX - imgRect.left);
//     const clickY = Math.floor(e.clientY - imgRect.top);
//     const x = Math.floor(clickX * (size.width / imgRect.width));
//     const y = Math.floor(clickY * (size.height / imgRect.height));

//     // Calculate gps coordinates from x and y coordinates
//     // (right - left) * (x / mapWidth)
//     const relXGps = (extend[2] - extend[0]) * (x / size.width);
//     const xGps = relXGps + extend[0];

//     // (top - bottom) * ((mapHeight - y) / mapHeight)
//     const relYGps = (extend[1] - extend[3]) *
//         ((size.height - y) / size.height);
//     const yGps = relYGps + extend[3];

//     mapConfig.currentGps = [xGps, yGps];
    
//     drawGpsDot(canvas, mapConfig);
// }

// export function getMapSize(url: string) {
//     return new Promise<{ width: number, height: number }>(resolve => {
//         const image = new Image();
//         image.addEventListener("load", () => {
//             const size = { width: image.naturalWidth, height: image.naturalHeight };
//             image.remove();
//             resolve(size);
//         });
//         image.src = url;
//         document.body.append(image);
//     });
// }

// export function getCurrentGps() {
//     return mapConfig.currentGps;
// }