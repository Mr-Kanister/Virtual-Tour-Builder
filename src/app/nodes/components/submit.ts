import { VirtualTourNode } from "@photo-sphere-viewer/virtual-tour-plugin";
import { getElementById, getImageSize } from "../../utils/dom";

export function basic(successUrl?: string) {
    return async (e: Event) => {
        e.preventDefault();

        const form = e.currentTarget as HTMLFormElement | null;
        if (!form) {
            throw new Error("current target is empty");
        }

        const data = new FormData(form);
        const patch = {
            name: data.get("name") ?? undefined,
            description: data.get("description") ?? undefined,
            caption: data.get("caption") ?? undefined,
            gps: data.get("gps") ?? undefined,
        }

        const res = await fetch(form.action, {
            method: "PATCH",
            body: JSON.stringify(patch),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
        });
        if (!res.ok) {
            alert(`Error: ${res.status}`);
        } else if (!successUrl) {
            window.location.reload();
        } else {
            window.location.assign(successUrl);
        }
    }
}

export async function panorama(e: Event) {
    e.preventDefault();

    const form = e.currentTarget as HTMLFormElement | null;
    if (!form) {
        throw new Error("current target is empty");
    }

    const data = new FormData(form);

    // get size of image to be uploaded
    const image = data.get("image");
    if (image instanceof File) {
        const size = await getImageSize(URL.createObjectURL(image));
        data.set("size", JSON.stringify(size));
    }

    const res = await fetch(form.action, {
        method: "PUT",
        body: data,
    });

    if (!res.ok) {
        alert(`Error: ${res.status}`);
    } else {
        window.location.reload();
    }
}

export function sphereCorrection(successUrl: string) {
    return async (e: Event) => {
        e.preventDefault();

        const form = e.currentTarget as HTMLFormElement | null;
        if (!form) {
            throw new Error("current target is empty");
        }
        const data = new FormData(form);
        const patch = {
            sphereCorrection: `{
            "pan": ${getElementById<HTMLInputElement>("sphere-correction-pan").value},
            "tilt": ${data.get("tilt")},
            "roll": ${data.get("roll")}
        }`,
        };

        const res = await fetch(form.action, {
            method: "PATCH",
            body: JSON.stringify(patch),
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
        });

        if (!res.ok) {
            alert(`Error: ${res.status}`);
        } else {
            window.location.assign(successUrl);
        }
    }
}

export function hotspot(nodeId: number) {
    return async (e: Event) => {
        e.preventDefault();

        const form = e.currentTarget as HTMLFormElement | null;
        if (!form) {
            throw new Error("current target is empty");
        }
        const formData = new FormData(form);

        let res: Response;
        if (formData.get("hotspot") === "1") {
            res = await fetch(form.action, {
                method: "POST",
                body: JSON.stringify({
                    node: nodeId,
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                },
            });
        } else {
            res = await fetch(form.action, {
                method: "DELETE",
            });
        }

        if (!res.ok) {
            alert(`Error: ${res.status}`);
        } else {
            window.location.reload();
        }
    };
}