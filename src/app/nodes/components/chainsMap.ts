import L from "leaflet";

const greenIcon = new L.Icon({
    iconUrl: "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});

const markers = new Map<string, L.Marker>();

export function initMap(container: string | HTMLElement, gps?: [number, number]) {
    if (!gps) {
        gps = [49, 11];
    }

    // leaflet uses [lat, lng]
    // psv uses [lng, lat]
    gps = [gps[1], gps[0]];

    const map = L.map(container).setView(gps, 13);

    L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 19,
        attribution: `&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>`
    })
        .addTo(map);

    L.marker(gps)
        .addTo(map);

    return map;
}

export function addLinks(links: any[], callback: Function) {
    links.forEach(link => {
        let gps = JSON.parse(link.gps);
        // leaflet uses [lat, lng]
        // psv uses [lng, lat]
        gps = [gps[1], gps[0]];

        markers.set(link.node_to, L.marker(gps, {
            icon: greenIcon,
        })
            .bindTooltip(`${link.name}`)
            .addEventListener("click", () => callback(link))
        );
    });
}

export function showLinks(map: L.Map) {
    markers.forEach(marker => marker.addTo(map));
}

export function hideLinks(map: L.Map) {
    markers.forEach(marker => marker.removeFrom(map));
}

export function hideLink(map: L.Map, node_to: string) {
    markers.get(node_to)?.removeFrom(map);
}

export function showLink(map: L.Map, node_to: string) {
    markers.get(node_to)?.addTo(map);
}