import { VirtualTourLink, VirtualTourPlugin } from "@photo-sphere-viewer/virtual-tour-plugin";
import L from "leaflet";

const currentLinks = new Map<string, L.Marker>();
const removedLinks = new Map<string, L.Marker>();
const addableLinks = new Map<string, L.Marker>();

const greenIcon = new L.Icon({
    iconUrl: "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-green.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
const redIcon = new L.Icon({
    iconUrl: "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-red.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
const blueIcon = new L.Icon({
    iconUrl: "https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-blue.png",
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});


export function initMap(container: string | HTMLElement, gps?: [number, number]) {
    if (!gps) {
        gps = [49, 11];
    }

    // leaflet uses [lat, lng]
    // psv uses [lng, lat]
    gps = [gps[1], gps[0]];

    const map = L.map(container).setView(gps, 13);

    L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom: 19,
        attribution: `&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>`
    })
        .addTo(map);

    L.marker(gps, { icon: blueIcon })
        .addTo(map);

    return map;
}

export function addLink(map: L.Map, row: HTMLTableRowElement) {
    let targetGps = JSON.parse(row.attributes.getNamedItem("target-gps")!.value) as [number, number];

    // leaflet uses [lat, lng]
    // psv uses [lng, lat]
    targetGps = [targetGps[1], targetGps[0]];

    currentLinks.set(row.attributes.getNamedItem("target-id")!.value, L.marker(targetGps, {
        title: row.firstElementChild?.textContent?.trim() ?? "",
        icon: blueIcon,
    })
        .addTo(map));
}

export function showAddableLink(
    map: L.Map, node: any, callback: Function,
    currentNodeId: string, table: HTMLTableElement, virtualTourPlugin: VirtualTourPlugin,
    links: VirtualTourLink[]
) {
    const nodeId = node.id === "-1" ? node.data.loadTour.startNodeId : node.id;
    
    if (currentLinks.has(nodeId)) return;

    let gps = JSON.parse(node.gps);
    // leaflet uses [lat, lng]
    // psv uses [lng, lat]
    gps = [gps[1], gps[0]];

    addableLinks.set(nodeId, L.marker(gps, {
        icon: greenIcon,
    })
        .addTo(map)
        .bindTooltip(`Add ${node.name}`)
        // currentNodeId: string, map: L.Map, table: HTMLTableElement,
        // virtualTourPlugin: VirtualTourPlugin, links: VirtualTourLink[], node: any
        .addOneTimeEventListener("click",
            callback(currentNodeId, map, table, virtualTourPlugin, links, node)
        ));
}

export function hideAddableLinks(map: L.Map) {
    addableLinks.forEach(marker => marker.removeFrom(map));
    addableLinks.clear();
}

export function hideRemovedLinks(map: L.Map) {
    removedLinks.forEach(marker => marker.removeFrom(map));
}
export function showRemovedLinks(map: L.Map) {
    // if link is readded, don't show it's removed one
    for (const link of removedLinks.entries()) {
        if (currentLinks.has(link[0])) continue;

        link[1].addTo(map);
    }
}

export function removeLink(map: L.Map, targetId: string) {
    const marker = currentLinks.get(targetId);
    if (!marker) throw new Error(`Link with target ${targetId} not previously in map`);

    marker.setIcon(redIcon);

    currentLinks.delete(targetId);
    removedLinks.set(targetId, marker);
}