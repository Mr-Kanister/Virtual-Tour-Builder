import { getElementById } from "../utils/dom";

export { };

async function send(e: Event) {
    e.preventDefault();

    const form = e.currentTarget as HTMLFormElement | null;
    if (!form) {
        throw new Error("current target is empty");
    }

    const data = new FormData(form);
    const submit = {
        name: data.get("name"),
        description: data.get("description") !== "" ? data.get("description") : undefined,
    }

    const res = await fetch(form.action, {
        method: "POST",
        body: JSON.stringify(submit),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    });
    if (!res.ok) {
        alert(`Error: ${res.status}`);
    } else {
        const { id: id } = await res.json();
        window.location.assign(`/app/nodes/${id}`);
    }
}

function add(e: Event) {
    // disable button
    const addNodeButton = e.currentTarget as HTMLButtonElement | null;
    if (!addNodeButton) throw new Error("current target is empty");
    addNodeButton.disabled = true;

    // show edit form
    const form = document.getElementById("add-node-form");
    if (!form) {
        throw new Error("add-node-form not found");
    }
    form.classList.remove("hidden");

    // focus input field
    const firstChild = form.children[1]?.firstElementChild;
    if (firstChild instanceof HTMLInputElement) {
        firstChild.focus();
        firstChild.select();
    }

    // register event listener for form submission
    form.addEventListener("submit", send)
}

async function getVirtualTour() {
    const tourId = getElementById<HTMLInputElement>("tour-id").value;
    if (tourId === "") return;

    const res = await fetch(`/api/plugins?tour=${tourId}`);
    if (!res.ok) {
        alert(`Error: ${res.status}`);
        return;
    }
    const plugins = await res.json();
    const virtualTour = plugins.find((p: {
        id: number,
        name: string,
        config: string,
        tour: number,
    }) => p.name === "virtual-tour");

    return virtualTour;
}

async function handleStartnodeCheckbox(e: Event) {
    if (!(e.target instanceof HTMLInputElement)) return;
    if (e.target.getAttribute("name") !== "startnode") return;

    const virtualTour = await getVirtualTour();
    if (!virtualTour) return;

    const virtualTourConfigJson = (await getVirtualTour()).config as string;

    const virtualTourConfig = JSON.parse(virtualTourConfigJson);
    if (!virtualTourConfig) return;

    if (e.target.checked) {
        // set as startnode
        virtualTourConfig.startNodeId = e.target.closest("a")!.getAttribute("node-id")!;
    } else {
        // unset startnode
        virtualTourConfig.startNodeId = undefined;
    }

    const res = await fetch(`/api/plugins/${virtualTour.id}`, {
        method: "PATCH",
        body: JSON.stringify({
            config: JSON.stringify(virtualTourConfig),
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        },
    });

    if (!res.ok) {
        alert(`Error: ${res.status}`);
        return;
    } else {
        window.location.reload();
    }
}

function filter(filter: string) {
    const table = getElementById<HTMLDivElement>("node-table");
    const children = table.children;

    for (let i = 2; i < children.length; i++) {
        const row = children[i] as HTMLAnchorElement;
        const name = (row.children[1] as HTMLDivElement).textContent;
        const caption = (row.children[2] as HTMLDivElement).textContent;
        if ((name?.toUpperCase().indexOf(filter.toUpperCase()) ?? -1) > -1 ||
            (caption?.toUpperCase().indexOf(filter.toUpperCase()) ?? -1) > -1) {
            row.style.display = "";
        } else {
            row.style.display = "none";
        }
    }
}

const addNodeButton = document.getElementById("add-tour") as HTMLButtonElement | null;
if (addNodeButton)
    addNodeButton.addEventListener("click", add, { once: true });

getElementById("node-table").addEventListener("input", handleStartnodeCheckbox);

filter(getElementById<HTMLInputElement>("filter").value);
getElementById("filter").addEventListener("input", e => filter((e.target as HTMLInputElement).value));