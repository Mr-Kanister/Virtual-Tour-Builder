GET /api/tours(?tour=name)
    Get all tours or a specific one in an array
GET /api/tours/name
    Get a specific tour

GET /api/nodes(?tour=name)
    Get all nodes in the specified tour
GET /api/nodes/id
    Get a specific node

GET /api/links(?tour=name)
    Get all links in the specified tour
GET /api/links/id
    Get a specific link

GET /api/markers(?tour=name)
    Get all markers in the specified tour
GET /api/markers/id
    Get a specific marker

---

POST /api/tours/
    Post a new tour. Tours are not updatable.

POST /api/nodes/tour
    Post a new node in the specified tour.

POST /api/links
    Post a new link.

POST /api/markers
    Post a new marker.

---

PATCH /api/nodes/id
    Update a node.

PATCH /api/markers/id
    Update a marker.

Patch /api/links/id
    Update a link.

---

PUT /api/upload/panorama
    Upload panorama as multipart. image, size and node required.

---

DELETE /api/tours/name
    Delete a tour

DELETE /api/nodes/id
    Delete a node

DELTE /api/links/id
    Delete a link

DELETE /api/markers
    Delete a markers
